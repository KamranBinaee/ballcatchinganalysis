#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 17:31:37 2017

@author: kamranbinaee
"""

from __future__ import division

import sys
import PerformParser as pp
import pandas as pd
import numpy as np
import scipy
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


gazeX = np.array([])
gazeY = np.array([])
handX = np.array([])
handY = np.array([])
handZ = np.array([])
myColor = np.array([])

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']

# Best Sbjects
fileTimeList = [ '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-6-13-4' ]


# Worst Sbjects
fileTimeList = ['2016-4-19-14-4', '2016-4-27-13-28', '2016-5-5-13-7']

#fileTimeList = [ '2016-5-6-13-4']

#fileTimeList = ['2016-5-3-12-52']

#fileTimeList = ['2016-5-5-13-7']
#fileTime = '2016-4-19-14-4'

def myPolyfit(x, y, degree):
    # Polynomial Regression
    x = np.array(x, dtype = float)
    y = np.array(y, dtype = float)
    results = {}

    coeffs = np.polyfit(x, y, degree)

     # Polynomial Coefficients
    results['polynomial'] = coeffs.tolist()

    # r-squared
    p = np.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = np.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = np.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = np.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    results['determination'] = ssreg / sstot

    return results

def myFunc(timeOffset):
    gazeX = np.array([])
    gazeY = np.array([])
    handX = np.array([])
    handY = np.array([])
    handZ = np.array([])
    myColor = np.array([])

    for fileTime in fileTimeList: 
        print('.... File Time : ', fileTime)
        expCfgName = "gd_pilot.cfg"
        sysCfgName = "PERFORMVR.cfg"
         
        filePath = "../Data/" + fileTime + "/"
        fileName = "exp_data-" + fileTime
         
        sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
         
        rawDataFrame = sessionDict['raw']
        processedDataFrame = sessionDict['processed']
        calibDataFrame = sessionDict['calibration']
        trialInfoDataFrame = sessionDict['trialInfo']
        
        #print(list(processedDataFrame.columns))
        
        
        #==============================================================================
        
        #==============================================================================
        #print ('Reading the All Subject Pickle File')
        #df = pd.read_pickle('../Data/AllSubjects_2.pickle')
        #rawDataFrame = df['raw']
        #processedDataFrame = df['processed']
        #calibDataFrame = df['calibration']
        #trialInfoDataFrame = df['trialInfo']
        # 
        #==============================================================================
        #xErr = processedDataFrame.gazeError_WCS.X.values
        #yErr = processedDataFrame.gazeError_WCS.Y.values
        #zErr = processedDataFrame.gazeError_WCS.Z.values
        
        offset = 1
        #print(len(processedDataFrame.gazeError_WCS.X.values[trialInfoDataFrame.ballOnIdx.values]))
        #print(len(processedDataFrame.gazeError_WCS.Y.values[trialInfoDataFrame.ballOnIdx.values]))
        xVel = rawDataFrame.paddlePos.X.diff()/(1/75.)
        yVel = rawDataFrame.paddlePos.Y.diff()/(1/75.)
        zVel = rawDataFrame.paddlePos.Z.diff()/(1/75.)
    
        for i in range(offset):
            gazeX = np.hstack((gazeX, processedDataFrame.gazeError_WCS.X.values[trialInfoDataFrame.ballOnIdx.values+i]))
            gazeY = np.hstack((gazeY, processedDataFrame.gazeError_WCS.Y.values[trialInfoDataFrame.ballOnIdx.values+i]))
    
            #handX = np.hstack((handX, xVel[trialInfoDataFrame.ballOnIdx.values+timeOffset]))# - rawDataFrame.ballPos.X.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
            #handY = np.hstack((handY, yVel[trialInfoDataFrame.ballOnIdx.values+timeOffset]))# - rawDataFrame.ballPos.Y.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
            #handZ = np.hstack((handZ, zVel[trialInfoDataFrame.ballOnIdx.values+timeOffset]))# - rawDataFrame.ballPos.Z.values[trialInfoDataFrame.ballCrossingIndex.values+i]))

            handX = np.hstack((handX, rawDataFrame.paddlePos.X.values[trialInfoDataFrame.ballCrossingIndex.values+timeOffset] - rawDataFrame.ballPos.X.values[trialInfoDataFrame.ballCrossingIndex.values+timeOffset]))
            handY = np.hstack((handY, rawDataFrame.paddlePos.Y.values[trialInfoDataFrame.ballCrossingIndex.values+timeOffset] - rawDataFrame.ballPos.Y.values[trialInfoDataFrame.ballCrossingIndex.values+timeOffset]))
            handZ = np.hstack((handZ, rawDataFrame.paddlePos.Z.values[trialInfoDataFrame.ballCrossingIndex.values+timeOffset] - rawDataFrame.ballPos.Z.values[trialInfoDataFrame.ballCrossingIndex.values+timeOffset]))
            
            #handX = np.hstack((handX, rawDataFrame.paddlePos.X.values[trialInfoDataFrame.ballCrossingIndex.values+i] - rawDataFrame.ballPos.X.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
            #handY = np.hstack((handY, rawDataFrame.paddlePos.Y.values[trialInfoDataFrame.ballCrossingIndex.values+i] - rawDataFrame.ballPos.Y.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
            #handZ = np.hstack((handZ, rawDataFrame.paddlePos.Z.values[trialInfoDataFrame.ballCrossingIndex.values+i] - rawDataFrame.ballPos.Z.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
        
        myColor = np.hstack((myColor, trialInfoDataFrame.ballCaughtQ.values))
        
    return gazeX, gazeY, handX, handY, handZ, myColor

#distance = np.power(np.power(handX,2)+np.power(handY,2)+np.power(handZ,2),0.5)

def myPlot(myX, myY, trialIndex, xLabel, yLabel, myTitle, fileName):
    
    xlim = 12
    ylim = 8
    plt.figure()
    xSuccess = myX[trialIndex==True]
    ySuccess = myY[trialIndex == True]
    plt.plot(xSuccess, ySuccess, '.b')
    m,b = np.polyfit(xSuccess, ySuccess, 1) 
    x = np.arange(-xlim,xlim)
    plt.plot(x, m*x+b, '--b') 

    xFail = myX[trialIndex==False]
    yFail = myY[trialIndex == False]
    plt.plot(xFail, yFail, '.r')
    m,b = np.polyfit(xFail, yFail, 1) 
    x = np.arange(-xlim,xlim)
    plt.plot(x, m*x+b, '--r') 
    
    degree = 1
    m = np.polyfit(myX, myY, degree) 
    p = np.poly1d(m)
    x = np.arange(-xlim,xlim)
    y = p(x)
    plt.plot(x, y, '--k')
    
    print('MeanX = ', np.mean(myX), ' STDX = ',  np.std(myX) )
    print('MeanY = ', np.mean(myY), ' STDY = ',  np.std(myY) )
    


    #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
    #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
    plt.xlim(-xlim,xlim)
    plt.ylim(-ylim,ylim)
    plt.grid(True)
    plt.title(myTitle)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    
    results = myPolyfit(xFail, yFail, degree)
    rSquared = results['determination']
    plt.text(7, 5, r'$R^2 = $' + '%.2f'%(rSquared) + '\n' + r'$m = $'+ '%.2f'%(m[0]), fontsize=14)

    results = myPolyfit(xSuccess, ySuccess, degree)
    rSquared = results['determination']
    plt.text(-7, 5, r'$R^2 = $' + '%.2f'%(rSquared) + '\n' + r'$m = $'+ '%.2f'%(m[0]), fontsize=14)

    currentFile = os.getcwd()
    #print(currentFile)
    plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'.png',dpi=800)
    plt.show()
    
    #plt.close()

#myPlot(gazeX, 10*handX, myColor, 'Gaze Prediction Error AZ', 'Hand Velocity X [cm/s]', 'Effect of Gaze Prediction (AZ) on Hand Velocity (X)', 'GazeXHandX_Bad')

#myPlot(abs(gazeX), abs(handY), myColor, 'Gaze Prediction Error Az', 'Hand Position Error Y', 'Effect of Gaze Prediction (AZ) on Hand Position (Y)')
#myPlot(abs(gazeX), abs(handZ), myColor, 'Gaze Prediction Error Az', 'Hand Position Error Z', 'Effect of Gaze Prediction (AZ) on Hand Position (Z)')

gazeX, gazeY, handX, handY, handZ, myColor = myFunc(0)
#myPlot(gazeY, 10*handY, myColor, 'Gaze Prediction Error EL', 'Hand Velocity Y [cm/s]', 'Effect of Gaze Prediction (EL) on Hand Velocity (Y)', 'GazeYHandY_Bad')
#myPlot(gazeX, 10*handX, myColor, 'Gaze Prediction Error AZ', 'Hand Velocity X [cm/s]', 'Effect of Gaze Prediction (AZ) on Hand Velocity (X)', 'GazeXHandX_Bad')

handX = np.array(handX, dtype = float)
handY = np.array(handY, dtype = float)
handZ = np.array(handZ, dtype = float)
gE = np.sqrt(np.power(gazeX,2) + np.power(gazeY,2))
hE = np.sqrt(np.power(10*handX,2) + np.power(10*handY,2)+ np.power(10*handZ,2))
myPlot(gE, hE, myColor, 'Gaze Prediction Error (Degree)', 'Hand Position Error [cm]', 'Effect of Gaze Prediction Error on Hand Position', 'GazeHandABS_Bad')


#myPlot(abs(gazeY), abs(handX), myColor, 'Gaze Prediction Error EL', 'Hand Position Error X', 'Effect of Gaze Prediction (EL) on Hand Position (X)')
#myPlot(abs(gazeY), abs(handZ), myColor, 'Gaze Prediction Error EL', 'Hand Position Error Z', 'Effect of Gaze Prediction (EL) on Hand Position (Z)')

sys.exit()

myM1 = []
myB1 = []
myM2 = []
myB2 = []
T = np.arange(23)
T = np.arange(19,20)
for i in T:
    gazeX, gazeY, handX, handY, myColor = myFunc(i)
    m,b = np.polyfit(gazeX, 10*handX, 1) 
    myM1.append(m)
    myB1.append(b)
    m,b = np.polyfit(gazeY, 10*handY, 1) 
    myM2.append(m)
    myB2.append(b)
print(myM1)
print(myB1)
print(myM2)
print(myB2)

plt.figure()
plt.plot(T/75.0, myM1, '-ob', label = 'Slope for GazeX-HandX')
plt.plot(T/75.0, myM2, '-or', label = 'Slope for GazeY-HandY') 

#plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
#plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
#plt.xlim(-12,12)
#plt.ylim(-40,40)
plt.grid(True)
plt.title('Effect of Prediction Legnth on Slope')
plt.xlabel('Prediction Time [s]')
plt.ylabel('Line Slope')
plt.legend()
currentFile = os.getcwd()
#print(currentFile)
plt.savefig(currentFile+'/Outputs/HandVelocityFigures/slopeForGoodSlope.png',dpi=800)

plt.show()

plt.figure()
plt.plot(T/75.0, myB1, '-.b', label = 'Intercept GazeX-HandX') 
plt.plot(T/75.0, myB2, '-.r', label = 'Intercept GazeY-HandY')  
plt.grid(True)
plt.title('Effect of Prediction Legnth on Regression Intercept')
plt.xlabel('Prediction Time [s]')
plt.ylabel('Line Intercept')
plt.legend()
currentFile = os.getcwd()
#print(currentFile)
plt.savefig(currentFile+'/Outputs/HandVelocityFigures/slopeForGoodIntercept.png',dpi=800)

plt.show()

#myPlot(abs(handY), abs(gazeY), myColor, 'Hand Position Error Y', 'Gaze Prediction Error El', 'Effect of Gaze Prediction (EL) on Hand Position (Y)')

#myPlot(abs(distance), abs(gazeX), myColor, 'Hand-Paddle Distance', 'Gaze Prediction Error Az', 'Effect of Gaze Prediction (AZ) on Hand-Paddle Distance')
#myPlot(abs(distance), abs(gazeY), myColor, 'Hand-Paddle Distance', 'Gaze Prediction Error El', 'Effect of Gaze Prediction (EL) on Hand-Paddle Distance')
#myPlot(handX, gazeX, 'Hand Position X', 'Gaze Prediction Error Az', 'Effect of Gaze Prediction (AZ) on Hand Position (X)')
#myPlot(handY, gazeY, 'Hand Position Y', 'Gaze Prediction Error El', 'Effect of Gaze Prediction (EL) on Hand Position (Y)')



