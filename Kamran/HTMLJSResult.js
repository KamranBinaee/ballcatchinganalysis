HTML   JS  Result
Edit on 
// Made with Plotly's postMessage API
// https://github.com/plotly/postMessage-API

function changeType(type){
    var plot = document.getElementById('plot').contentWindow;
    plot.postMessage( {
    'task': 'restyle',
        'update': {'type': type},
    },
    'https://plot.ly');
}

function changeColor(colorscale){
    var plot = document.getElementById('plot').contentWindow;
    plot.postMessage( {
    'task': 'restyle',
        'update': {'colorscale': colorscale},
    },
    'https://plot.ly');
}

function reverse(){
    var plot = document.getElementById('plot').contentWindow;
    
    plot.postMessage({
    task: 'getAttributes',
        attributes: [ 'data[0].reversescale' ] },
    'https://plot.ly/');
    
    window.addEventListener('message', function(e) {
        var message = e.data;
        var reverse = message.response['data[0].reversescale'];
        
        if( reverse === undefined || reverse == false ){
            reverse = true;
        } 
        else if( reverse == true ){
            reverse = false;
        }
        
        plot.postMessage( {
        'task': 'restyle',
            'update': {'reversescale': reverse},
        },
        'https://plot.ly');
    });
}

function newPlot(){
    var plotURL = document.getElementById('plotURL').value + '.embed';
    var iframe = document.getElementById('plot');
    iframe.src = plotURL;   
}