def gazeVelocityAnalysis(trialInfoDataFrame, processedDataFrame, preBD, BD, postBD):
    T = 13.333
    numberOfPreBDFrames = int(np.round(preBD/T))
    numberOfPostBDFrames = int(np.round(postBD/T))
    numberOfBDFrames = int(np.round(BD/T))
    print numberOfPreBDFrames, numberOfBDFrames, numberOfPostBDFrames

    successGazeVelocity_PreBD = np.zeros(numberOfPreBDFrames, dtype = float)
    successBallVelocity_PreBD = np.zeros(numberOfPreBDFrames, dtype = float)
    successGazeError_PreBD = np.zeros(numberOfPreBDFrames, dtype = float)

    missGazeVelocity_PreBD = np.zeros(numberOfPreBDFrames, dtype = float)
    missBallVelocity_PreBD = np.zeros(numberOfPreBDFrames, dtype = float)
    missGazeError_PreBD = np.zeros(numberOfPreBDFrames, dtype = float)

    successGazeVelocity_BD = np.zeros(numberOfBDFrames, dtype = float)
    successBallVelocity_BD = np.zeros(numberOfBDFrames, dtype = float)
    successGazeError_BD = np.zeros(numberOfBDFrames, dtype = float)

    missGazeVelocity_BD = np.zeros(numberOfBDFrames, dtype = float)
    missBallVelocity_BD = np.zeros(numberOfBDFrames, dtype = float)
    missGazeError_BD = np.zeros(numberOfBDFrames, dtype = float)

    successGazeVelocity_PostBD = np.zeros(numberOfPostBDFrames, dtype = float)
    successBallVelocity_PostBD = np.zeros(numberOfPostBDFrames, dtype = float)
    successGazeError_PostBD = np.zeros(numberOfPostBDFrames, dtype = float)

    missGazeVelocity_PostBD = np.zeros(numberOfPostBDFrames, dtype = float)
    missBallVelocity_PostBD = np.zeros(numberOfPostBDFrames, dtype = float)
    missGazeError_PostBD = np.zeros(numberOfPostBDFrames, dtype = float)

    count = 0
    myList = []
    for i in range(len(trialInfoDataFrame)):
        if (abs(trialInfoDataFrame.preBlankDur.values[i] - preBD/1000.) < 0.001 and 
            abs(trialInfoDataFrame.postBlankDur.values[i] - postBD/1000.) < 0.001):
            tempVar = processedDataFrame.cycGazeVelocity.values[trialStartIdx[i]:ballOffIdx[i]]
            if (len(tempVar) < numberOfPreBDFrames):
                padSize = numberOfPreBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfPreBDFrames):
                tempVar = tempVar[:numberOfPreBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successGazeVelocity_PreBD = np.vstack((successGazeVelocity_PreBD, tempVar))
            else:
                missGazeVelocity_PreBD = np.vstack((missGazeVelocity_PreBD, tempVar))

            tempVar = processedDataFrame.ballVelocity.values[trialStartIdx[i]:ballOffIdx[i]]
            if (len(tempVar) < numberOfPreBDFrames):
                padSize = numberOfPreBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfPreBDFrames):
                tempVar = tempVar[:numberOfPreBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successBallVelocity_PreBD = np.vstack((successBallVelocity_PreBD, tempVar)) 
            else:
                missBallVelocity_PreBD = np.vstack((missBallVelocity_PreBD, tempVar)) 

            tempVar = processedDataFrame.gazeAngularError.values[trialStartIdx[i]:ballOffIdx[i]]
            if (len(tempVar) < numberOfPreBDFrames):
                padSize = numberOfPreBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfPreBDFrames):
                tempVar = tempVar[:numberOfPreBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successGazeError_PreBD = np.vstack((successGazeError_PreBD, tempVar))
            else:
                missGazeError_PreBD = np.vstack((missGazeError_PreBD, tempVar))

            tempVar = processedDataFrame.cycGazeVelocity.values[ballOffIdx[i]:ballOnIdx[i]]
            if (len(tempVar) < numberOfBDFrames):
                padSize = numberOfBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfBDFrames):
                tempVar = tempVar[:numberOfBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successGazeVelocity_BD = np.vstack((successGazeVelocity_BD, tempVar))
            else:
                missGazeVelocity_BD = np.vstack((missGazeVelocity_BD, tempVar))

            tempVar = processedDataFrame.ballVelocity.values[ballOffIdx[i]:ballOnIdx[i]]
            if (len(tempVar) < numberOfBDFrames):
                padSize = numberOfBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfBDFrames):
                tempVar = tempVar[:numberOfBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successBallVelocity_BD = np.vstack((successBallVelocity_BD, tempVar)) 
            else:
                missBallVelocity_BD = np.vstack((missBallVelocity_BD, tempVar))

            tempVar = processedDataFrame.gazeAngularError.values[ballOffIdx[i]:ballOnIdx[i]]
            if (len(tempVar) < numberOfBDFrames):
                padSize = numberOfBDFrames - len(tempVar)
                b = np.lib.pad(tempVar, padSize, 'constant', constant_values = np.nan)
                tempVar = b[padSize:]
            elif(len(tempVar) > numberOfBDFrames):
                tempVar = tempVar[:numberOfBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successGazeError_BD = np.vstack((successGazeError_BD, tempVar))
            else:
                missGazeError_BD = np.vstack((missGazeError_BD, tempVar))

            tempVar = processedDataFrame.cycGazeVelocity.values[ballOnIdx[i]: ballOnIdx[i] + numberOfPostBDFrames]
            if (len(tempVar) < numberOfPostBDFrames):
                padSize = numberOfPostBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfPostBDFrames):
                tempVar = tempVar[:numberOfPostBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successGazeVelocity_PostBD = np.vstack((successGazeVelocity_PostBD, tempVar))
            else:
                missGazeVelocity_PostBD = np.vstack((missGazeVelocity_PostBD, tempVar))

            tempVar = processedDataFrame.ballVelocity.values[ballOnIdx[i]: ballOnIdx[i] + numberOfPostBDFrames]
            if (len(tempVar) < numberOfPostBDFrames):
                padSize = numberOfPostBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfPostBDFrames):
                tempVar = tempVar[:numberOfPostBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successBallVelocity_PostBD = np.vstack((successBallVelocity_PostBD, tempVar)) 
            else:
                missBallVelocity_PostBD = np.vstack((missBallVelocity_PostBD, tempVar)) 

            tempVar = processedDataFrame.gazeAngularError.values[ballOnIdx[i]: ballOnIdx[i] + numberOfPostBDFrames]
            if (len(tempVar) < numberOfPostBDFrames):
                padSize = numberOfPostBDFrames - len(tempVar)
                tempVar = np.lib.pad(tempVar, (0, padSize), 'constant', constant_values = np.nan)
            elif(len(tempVar) > numberOfPostBDFrames):
                tempVar = tempVar[:numberOfPostBDFrames]
            if (trialInfoDataFrame.ballCaughtQ.values[i] == True):
                successGazeError_PostBD = np.vstack((successGazeError_PostBD, tempVar))
            else:
                missGazeError_PostBD = np.vstack((missGazeError_PostBD, tempVar))

    successGazeVelocity_PreBD = np.delete(successGazeVelocity_PreBD, 0,0)
    successGazeVelocity_BD = np.delete(successGazeVelocity_BD, 0,0)
    successGazeVelocity_PostBD = np.delete(successGazeVelocity_PostBD, 0,0)

    missGazeVelocity_PreBD = np.delete(missGazeVelocity_PreBD, 0,0)
    missGazeVelocity_BD = np.delete(missGazeVelocity_BD, 0,0)
    missGazeVelocity_PostBD = np.delete(missGazeVelocity_PostBD, 0,0)

    successBallVelocity_PreBD = np.delete(successBallVelocity_PreBD, 0,0)
    successBallVelocity_BD = np.delete(successBallVelocity_BD, 0,0)
    successBallVelocity_PostBD = np.delete(successBallVelocity_PostBD, 0,0)

    missBallVelocity_PreBD = np.delete(missBallVelocity_PreBD, 0,0)
    missBallVelocity_BD = np.delete(missBallVelocity_BD, 0,0)
    missBallVelocity_PostBD = np.delete(missBallVelocity_PostBD, 0,0)

    successGazeError_PreBD = np.delete(successGazeError_PreBD, 0,0)
    successGazeError_BD = np.delete(successGazeError_BD, 0,0)
    successGazeError_PostBD = np.delete(successGazeError_PostBD, 0,0)

    missGazeError_PreBD = np.delete(missGazeError_PreBD, 0,0)
    missGazeError_BD = np.delete(missGazeError_BD, 0,0)
    missGazeError_PostBD = np.delete(missGazeError_PostBD, 0,0)

    successGazeVelocity_PreBD[successGazeVelocity_PreBD>50] = np.NaN
    successBallVelocity_PreBD[successGazeVelocity_PreBD>50] = np.NaN

    missGazeVelocity_PreBD[missGazeVelocity_PreBD>50] = np.NaN
    missBallVelocity_PreBD[missGazeVelocity_PreBD>50] = np.NaN

    successGazeVelocity_BD[successGazeVelocity_BD>50] = np.NaN
    successBallVelocity_BD[successGazeVelocity_BD>50] = np.NaN

    missGazeVelocity_BD[missGazeVelocity_BD>50] = np.NaN
    missBallVelocity_BD[missGazeVelocity_BD>50] = np.NaN
    
    successPG_PreBD = np.divide(successGazeVelocity_PreBD, successBallVelocity_PreBD)
    successPG_BD = np.divide(successGazeVelocity_BD, successBallVelocity_BD)
    successPG_PostBD = np.divide(successGazeVelocity_PostBD, successBallVelocity_PostBD)

    missPG_PreBD = np.divide(missGazeVelocity_PreBD, missBallVelocity_PreBD)
    missPG_BD = np.divide(missGazeVelocity_BD, missBallVelocity_BD)
    missPG_PostBD = np.divide(missGazeVelocity_PostBD, missBallVelocity_PostBD)


    return [successPG_PreBD, successPG_BD, successPG_PostBD,
            missPG_PreBD, missPG_BD, missPG_PostBD,
            successGazeError_PreBD, successGazeError_BD, successGazeError_PostBD,
            missGazeError_PreBD, missGazeError_BD, missGazeError_PostBD,]