# ====================================================================================================================
# ====================================================================================================================

from __future__ import division

import PerformParser as pp
import pandas as pd
import numpy as np
from scipy import signal as sig
import performFun as pF

import bokeh.plotting as bkP
import bokeh.models as bkM
from bokeh.palettes import Spectral6
bkP.output_notebook() 

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
from ipywidgets import interact
import filterpy as fP
from bokeh.io import push_notebook

import Quaternion as qu

import plotly
from plotly.graph_objs import Scatter, Layout
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot

print plotly.__version__

plotly.offline.init_notebook_mode()

import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D

bkP.output_notebook()

#bkP.output_file('timeSeries.html') 

#%pylab inline
#%matplotlib notebook

# ====================================================================================================================
# ====================================================================================================================

# List of subjects with good calibration quality
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
                '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']
fileTimeList = ['2016-4-19-14-4'] # 

rawDataFrame = pd.DataFrame()
processedDataFrame = pd.DataFrame()
calibDataFrame = pd.DataFrame()
trialInfoDataFrame =  pd.DataFrame()
#fileTime = '2016-4-22-11-57'
#fileTime = '2016-4-27-13-28'
#fileTime = '2016-4-28-10-57'
#fileTime = '2016-4-29-11-56'
#fileTime = '2016-5-3-12-52'
#fileTime = '2016-5-4-13-3'
#fileTime = '2016-5-5-13-7'
#fileTime = '2016-5-6-11-2'
#fileTime = '2016-5-6-13-4'
expCfgName = "gd_pilot.cfg"
sysCfgName = "PERFORMVR.cfg"

saveSubjectPickle = False
startFromScratch = True

if startFromScratch == True:
    for fileTime in fileTimeList:

        print 'extracting data for:', fileTime
        filePath = "../Data/exp/" + fileTime + "/"
        fileName = "exp_data-" + fileTime

        sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
        sessionDict['processed'], sessionDict['trialInfo'] = pF.calculateCrossingFrame(sessionDict['raw'], sessionDict['processed'], sessionDict['trialInfo'])
        rawDataFrame = rawDataFrame.append(sessionDict['raw'], ignore_index=True)
        processedDataFrame = processedDataFrame.append(sessionDict['processed'], ignore_index=True)
        calibDataFrame = calibDataFrame.append(sessionDict['calibration'], ignore_index=True)
        trialInfoDataFrame = trialInfoDataFrame.append(sessionDict['trialInfo'], ignore_index=True)
else:
    print 'Reading the All Subject Pickle File'
    df = pd.read_pickle('AllSubjects_2.pickle')
    rawDataFrame = df['raw']
    processedDataFrame = df['processed']
    calibDataFrame = df['calibration']
    trialInfoDataFrame = df['trialInfo']
    

if saveSubjectPickle == True:
    sessionDict['raw'] = rawDataFrame
    sessionDict['processed'] = processedDataFrame
    sessionDict['calibration'] = calibDataFrame
    sessionDict['trialInfo'] = trialInfoDataFrame
    pd.to_pickle(sessionDict, 'AllSubjects_2.pickle')
    print 'All Subject Pickle Saved'

processedDataFrame.loc[:, ('headVelocity','')] = pF.calculateHeadVelocity(rawDataFrame, trialID = None, plottingFlag = False)
processedDataFrame.loc[:, ('ballVelocity','')] = pF.calculateBallVelocity(rawDataFrame, processedDataFrame, trialID = None, plottingFlag = False)

trialStartIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'trialStart'].index.tolist()
ballOffIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'ballRenderOff'].index.tolist()
ballOnIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'ballRenderOn'].index.tolist()
ballOnPaddleIdx = processedDataFrame[processedDataFrame['eventFlag'] == 'ballOnPaddle'].index.tolist()

ballCrossingIdx = np.zeros(len(trialInfoDataFrame), dtype = int)
ballCrossingIdx[trialInfoDataFrame.ballCaughtQ.values == True] = processedDataFrame[processedDataFrame['eventFlag'] == 'ballOnPaddle'].index.tolist()
ballCrossingIdx[trialInfoDataFrame.ballCaughtQ.values == False] = processedDataFrame[processedDataFrame['eventFlag'] == 'ballCrossingPaddle'].index.tolist()

trialInfoDataFrame.loc[:, ('trialStartIdx','')] = trialStartIdx
trialInfoDataFrame.loc[:, ('ballOffIdx','')] = ballOffIdx
trialInfoDataFrame.loc[:, ('ballOnIdx','')] = ballOnIdx
trialInfoDataFrame.loc[:, ('ballCrossingIndex','')] = ballCrossingIdx
print 'Number of Successful Trials: ', len(ballOnPaddleIdx), 'out of', len(trialStartIdx)
print 'Done!'

frameRange = range(0,300)
X = rawDataFrame.ballPos.X.values[frameRange]
Y = rawDataFrame.ballPos.Y.values[frameRange]
Z = rawDataFrame.ballPos.Z.values[frameRange]
color = rawDataFrame.isBallVisibleQ.values[frameRange]
myX = X[color == True] 
myY = Y[color == True] 
myZ = Z[color == True] 

print '\nColor = ', color, '\n'
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d'spoti)
ax.scatter(myX, myZ, myY, label = 'Ball Visible', c = 'r', marker = 'o', size = 20)
myX = X[color == False] 
myY = Y[color == False] 
myZ = Z[color == False] 
ax.scatter(myX, myZ, myY, label = 'Ball Invisible', c = 'w', marker = 'o', size = 20)


X = rawDataFrame.paddlePos.X.values[frameRange]
Y = rawDataFrame.paddlePos.Y.values[frameRange]
Z = rawDataFrame.paddlePos.Z.values[frameRange]
#ax = fig.add_subplot(111, projection='3d')
ax.scatter(X, Z, Y, label = 'Hand', c = 'b', marker = 'o')

X = rawDataFrame.viewPos.X.values[frameRange]
Y = rawDataFrame.viewPos.Y.values[frameRange]
Z = rawDataFrame.viewPos.Z.values[frameRange]
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(X, Z, Y, label = 'Head Position', c = 'g', marker = 'o')


ax.set_xlabel('X [m]')
ax.set_ylabel('Z [m]')
ax.set_zlabel('Y [m]')

legend = plt.legend(loc=[0.8,0.8], shadow=True, fontsize='small')# 'upper center'
plt.show()


