#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 16:24:44 2017

@author: kamranbinaee
"""


from __future__ import division

import PerformParser as pp
import pandas as pd
import numpy as np
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


def reject_outliers(data):
    m = 2
    u = np.nanmean(data)
    s = np.nanstd(data)
    filtered = [e for e in data if (u - 2 * s < e < u + 2 * s)]
    return filtered

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']
 
# Best Sbjects
fileTimeList = [ '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-6-13-4' ]


# Worst Sbjects
fileTimeList = ['2016-4-19-14-4', '2016-4-27-13-28', '2016-5-5-13-7']

#fileTime = '2016-5-3-12-52'

finalMeanX_S = np.zeros((1,101), dtype = float)
finalMeanX_F = np.zeros((1,101), dtype = float)
finalMeanY_S = np.zeros((1,101), dtype = float)
finalMeanY_F = np.zeros((1,101), dtype = float)
finalSTDX_S = np.zeros((1,101), dtype = float)
finalSTDX_F = np.zeros((1,101), dtype = float)
finalSTDY_S = np.zeros((1,101), dtype = float)
finalSTDY_F = np.zeros((1,101), dtype = float)

for fileTime in fileTimeList: 
    expCfgName = "gd_pilot.cfg"
    sysCfgName = "PERFORMVR.cfg"
     
    filePath = "../Data/" + fileTime + "/"
    fileName = "exp_data-" + fileTime
     
    sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
     
    rawDataFrame = sessionDict['raw']
    processedDataFrame = sessionDict['processed']
    calibDataFrame = sessionDict['calibration']
    trialInfoDataFrame = sessionDict['trialInfo']
    
    #==============================================================================
    
    #==============================================================================
    #print ('Reading the All Subject Pickle File')
    #df = pd.read_pickle('../Data/AllSubjects_2.pickle')
    #rawDataFrame = df['raw']
    #processedDataFrame = df['processed']
    #calibDataFrame = df['calibration']
    #trialInfoDataFrame = df['trialInfo']
    # 
    #==============================================================================
    xVel = rawDataFrame.paddlePos.X.diff()/(1/75.)
    yVel = rawDataFrame.paddlePos.Y.diff()/(1/75.)
    zVel = rawDataFrame.paddlePos.Z.diff()/(1/75.)
    
    eyeToScreenDistance = 0.0725
    x = processedDataFrame.gazePoint.X.values
    x = np.array(x, dtype = float)
    gazeX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
    y = processedDataFrame.gazePoint.Y.values
    y = np.array(y, dtype = float)
    gazeY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
    x = processedDataFrame.ballOnScreen.X.values
    
    x = np.array(x, dtype = float)
    ballX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
    #y = processedDataFrame.rotatedBallOnScreen.Y.values[stIndex[trialID]:crIndex[trialID]]
    y = processedDataFrame.ballOnScreen.Y.values
    y = np.array(y, dtype = float)
    ballY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
    
    gb = trialInfoDataFrame.groupby(trialInfoDataFrame.postBlankDur)
    preBDList = [0.6, 0.8, 1.0]
    postBDList = [0.3, 0.4, 0.5]
    
    postBDList = [0.3]
    
    
    for postBD in postBDList:
        #plt.figure()
        fileName = 'PostBD = '+str(postBD)
        slicedDF = gb.get_group(postBD)
        #xPos = np.zeros((len(slicedDF),101),dtype = float)
        #yPos = np.zeros((len(slicedDF),101),dtype = float)
        #zPos = np.zeros((len(slicedDF),101),dtype = float)
        offsetPlus = int(postBD*75)
        offsetMinus = 101 - offsetPlus# 37.5
        #print (offsetPlus, offsetMinus)
        successIndex = np.where(slicedDF.ballCaughtQ.values == True)
        print(successIndex)
        failIndex = np.where(slicedDF.ballCaughtQ.values == False)
        print(failIndex)
        for cnt in range(len(slicedDF)):
            #i = slicedDF.trialStartIdx.values[cnt]
            #i = slicedDF.ballOffIdx.values[cnt]
            j = slicedDF.ballOnIdx.values[cnt]
            t = (np.arange(-100,1))/75
            ex = ballX[j-offsetMinus:j+offsetPlus] - gazeX[j-offsetMinus:j+offsetPlus]
            ey = ballY[j-offsetMinus:j+offsetPlus] - gazeY[j-offsetMinus:j+offsetPlus]
            
            if (slicedDF.ballCaughtQ.values[cnt] == True):
                finalMeanX_S = np.vstack((finalMeanX_S, ex))
                finalMeanY_S = np.vstack((finalMeanY_S, ey))
            else:
                finalMeanX_F = np.vstack((finalMeanX_F, ex))
                finalMeanY_F = np.vstack((finalMeanY_F, ey))
            

print(finalMeanY_S.shape)
print(finalMeanY_F.shape)
finalMeanX_S = np.delete(finalMeanX_S, 0,0)
finalMeanX_F = np.delete(finalMeanX_F, 0,0)

finalMeanY_S = np.delete(finalMeanY_S, 0,0)
finalMeanY_F = np.delete(finalMeanY_F, 0,0)


print(finalMeanY_S.shape)
print(finalMeanY_F.shape)

meanHandX_S = np.mean(abs(finalMeanX_S), 0)
meanHandX_F = np.mean(abs(finalMeanX_F), 0)    

meanHandY_S = np.mean(abs(finalMeanY_S), 0)
meanHandY_F = np.mean(abs(finalMeanY_F), 0)    

stdHandX_S = np.std(abs(finalMeanX_S), 0)
stdHandX_F = np.std(abs(finalMeanX_F), 0)

stdHandY_S = np.std(abs(finalMeanY_S), 0)
stdHandY_F = np.std(abs(finalMeanY_F), 0)

#plt.errorbar(t, meanHandX_S, yerr=stdHandX_S, fmt='.',color = 'b', label='Success',capthick=2, capsize = 2, alpha = 0.6)
#plt.errorbar(t, meanHandX_F, yerr=stdHandX_F, fmt='.',color = 'r', label='Fail',capthick=2 , capsize = 2,   alpha = 0.6)
plt.figure()
plt.plot(t, meanHandX_S, 'b')
plt.plot(t, meanHandX_F, 'r')
plt.fill_between(t, y1 = meanHandX_S - stdHandX_S , y2 = meanHandX_S + stdHandX_S ,color = 'b', label='Success', alpha = 0.6)
plt.fill_between(t, y1 = meanHandX_F - stdHandX_F , y2 = meanHandX_F + stdHandX_F ,color = 'r', label='Fail', alpha = 0.6)
plt.axvline(x=-postBDList[0], color='k', linestyle='--')
plt.axvline(x=-postBDList[0]-0.5, color='k', linestyle='--')
plt.ylim(-2,55)
plt.grid(True)
plt.legend(loc=[0.85,1.01])
plt.title("Gaze-Ball Azimuth Angle Vs. Time"+"\n"+fileName)
plt.xlabel('Time [s]')
plt.ylabel('Gaze-Ball Angle [degree]')
currentFile = os.getcwd()
#print(currentFile)
plt.savefig(currentFile+'/Outputs/GazeBallAngle/'+fileName +'_X.png',dpi=600)
#plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_X.svg', format='svg', dpi=1000)
#plt.show()
plt.close()

#plt.errorbar(t, meanHandY_S, yerr=stdHandY_S,color = 'b', label='Success', capthick=2, capsize = 2,  alpha = 0.6)
#plt.errorbar(t, meanHandY_F, yerr=stdHandY_F,color = 'r', label='Fail', capthick=2, capsize = 2, alpha = 0.6)
plt.figure()
plt.plot(t, meanHandY_S, 'b')
plt.plot(t, meanHandY_F, 'r')
plt.fill_between(t, y1 = meanHandY_S - stdHandY_S , y2 = meanHandY_S + stdHandY_S ,color = 'b', label='Success', alpha = 0.6)
plt.fill_between(t, y1 = meanHandY_F - stdHandY_F , y2 = meanHandY_F + stdHandY_F ,color = 'r', label='Fail', alpha = 0.6)
plt.axvline(x=-postBDList[0], color='k', linestyle='--')
plt.axvline(x=-postBDList[0]-0.5, color='k', linestyle='--')
plt.ylim(-2,55)
plt.grid(True)
plt.legend(loc=[0.85,1.01])
plt.title("Gaze-Ball Elevation Angle Vs. Time"+"\n"+fileName)
plt.xlabel('Time [s]')
plt.ylabel('Gaze-Ball Angle [degree]')
currentFile = os.getcwd()
#print(currentFile)
plt.savefig(currentFile+'/Outputs/GazeBallAngle/'+fileName +'_Y.png',dpi=600)
#plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_X.svg', format='svg', dpi=1000)
#plt.show()
plt.close()
