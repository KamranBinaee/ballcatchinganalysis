#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 17:16:40 2018

@author: kamranbinaee
"""


from __future__ import division

import sys
import PerformParser as pp
import pandas as pd
import numpy as np
import scipy
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']

fileTimeList = ['2016-4-19-14-4']
def dotproduct( v1, v2):
    r = sum((a*b) for a, b in zip(v1, v2))
    return r


def length(v):
    return np.sqrt(dotproduct(v, v))


def vectorAngle( v1, v2):
    r = (180.0/np.pi)*np.arccos((dotproduct(v1, v2)) / (length(v1) * length(v2)))#np.arccos((np.dot(v1,v2)/(np.linalg.norm(v1)*np.linalg.norm(v2))))#
    return r

def myPolyfit(x, y, degree):
    # Polynomial Regression
    x = np.array(x, dtype = float)
    y = np.array(y, dtype = float)
    results = {}

    coeffs = np.polyfit(x, y, degree)

     # Polynomial Coefficients
    results['polynomial'] = coeffs.tolist()

    # r-squared
    p = np.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = np.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = np.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = np.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    results['determination'] = ssreg / sstot

    return results

def myFunc(degree, postBD):
    blankSampleNumber = 38
    error_gazeX = np.zeros((blankSampleNumber))
    error_gazeY = np.zeros((blankSampleNumber))
    error_ballX = np.zeros((blankSampleNumber))
    error_ballY = np.zeros((blankSampleNumber))
    eyeToScreenDistance = 0.0725

    for fileTime in fileTimeList: 
        print('.... File Time : ', fileTime)
        expCfgName = "gd_pilot.cfg"
        sysCfgName = "PERFORMVR.cfg"
         
        filePath = "../Data/" + fileTime + "/"
        fileName = "exp_data-" + fileTime
         
        sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
         
        rawDataFrame = sessionDict['raw']
        processedDataFrame = sessionDict['processed']
        calibDataFrame = sessionDict['calibration']
        trialInfoDataFrame = sessionDict['trialInfo']

        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.preBlankDur, trialInfoDataFrame.postBlankDur])
        gb = trialInfoDataFrame.groupby(trialInfoDataFrame.postBlankDur)
        preBDList = [0.6, 0.8, 1.0]
        postBDList = [0.3, 0.4, 0.5]
        #postBD = 0.3
        fileName = 'PostBD = '+str(postBD)
        slicedDF = gb.get_group(postBD)
        #slicedDF = trialInfoDataFrame
        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.postBlankDur])
        #slicedDF = gb.get_group((0.3))
        crIndex = slicedDF.ballCrossingIndex.values
        stIndex = slicedDF.trialStartIdx.values
        ballOnIndex = slicedDF.ballOnIdx.values
        ballOffIndex = slicedDF.ballOffIdx.values
        for trialID in range(45):
        #trialID = 0
            x = processedDataFrame.gazePoint.X.values
            x = np.array(x, dtype = float)
            gazeX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
            y = processedDataFrame.gazePoint.Y.values
            y = np.array(y, dtype = float)
            gazeY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
    
            x = processedDataFrame.ballOnScreen.X.values
            x = np.array(x, dtype = float)
            ballX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
            #y = processedDataFrame.rotatedBallOnScreen.Y.values[stIndex[trialID]:crIndex[trialID]]
            y = processedDataFrame.ballOnScreen.Y.values
            y = np.array(y, dtype = float)
            ballY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
            print('Success?', slicedDF.ballCaughtQ.values[trialID])
            #idx = slicedDF.ballCrossingIndex.values[trialID]
            #print('GEX = ', ballX[idx] - gazeX[idx])
            #print('GEY = ', ballY[idx] - gazeY[idx])
            #idx = slicedDF.ballCrossingIndex.values
            #print('\nMinX = ', min(ballX[idx] - gazeX[idx]))
            #print('MeanX = ', np.mean(ballX[idx] - gazeX[idx]))
            #print('STDX = ', np.std(ballX[idx] - gazeX[idx]))
            #print('MaxX = ', max(ballX[idx] - gazeX[idx]))
            #print('Rate = ', np.sum(slicedDF.ballCaughtQ.values)/0.45)
    
            idx = slicedDF.ballCrossingIndex.values[trialID]
            frames = np.arange(stIndex[trialID],crIndex[trialID]+1)
            #xlim = 60
            #ylim = 50
            plt.figure()
            plt.plot(ballX[frames], ballY[frames], 'ob')
            plt.plot(gazeX[frames], gazeY[frames], 'xr')
            plt.plot(gazeX[idx], gazeY[idx], '*y')
            plt.plot(ballX[idx], ballY[idx], '*y')
            #idx = np.arange(slicedDF.ballOffIdx.values[trialID],slicedDF.ballOnIdx.values[trialID]+1)
            idx = np.arange(slicedDF.ballOffIdx.values[trialID], slicedDF.ballOffIdx.values[trialID] + blankSampleNumber)
            #print(min(idx), max(idx))
            #print('Len = ', len(idx))
            #plt.plot(ballX[idx], ballY[idx], '*b')
            #plt.plot(gazeX[idx], gazeY[idx], '*r')
    
            #degree = 2 
    
            dataX = gazeX[idx]
            dataY = gazeY[idx]
            coeffsX = np.polyfit(np.arange(len(dataX)), dataX, degree)
            coeffsY = np.polyfit(np.arange(len(dataY)), dataY, degree)
                    
            # r-squared
            pX = np.poly1d(coeffsX)
            pY = np.poly1d(coeffsY)
            # fit values, and mean
            xhat = pX(np.arange(len(dataX)))                         # or [p(z) for z in x]
            yhat = pY(np.arange(len(dataY)))
            errorX = np.abs(xhat - dataX)
            errorY = np.abs(yhat - dataY)        
            plt.plot(xhat,yhat, '-.k')
            error_gazeX = np.vstack((error_gazeX, errorX))
            error_gazeY = np.vstack((error_gazeY, errorY))
    
            dataX = ballX[idx]
            dataY = ballY[idx]
            coeffsX = np.polyfit(np.arange(len(dataX)), dataX, degree)
            coeffsY = np.polyfit(np.arange(len(dataY)), dataY, degree)
                    
            # r-squared
            pX = np.poly1d(coeffsX)
            pY = np.poly1d(coeffsY)
            # fit values, and mean
            xhat = pX(np.arange(len(dataX)))                         # or [p(z) for z in x]
            yhat = pY(np.arange(len(dataY)))
            errorX = np.abs(xhat - dataX)
            errorY = np.abs(yhat - dataY)        
            plt.plot(xhat,yhat, '-.k')
            error_ballX = np.vstack((error_ballX, errorX))
            error_ballY = np.vstack((error_ballY, errorY))
    
            #ybar = np.sum(y)/len(y)          # or sum(y)/len(y)
            #ssreg = np.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
            #sstot = np.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
            
            
            #plt.xlim(-30,xlim)
            #plt.ylim(-ylim,ylim)
            plt.grid(True)
            myText = 'Success Trial' if (slicedDF.ballCaughtQ.values[trialID] == True) else 'Fail Trial'
            plt.title('Gaze Vs Ball Angular Position : '+ str(myText))
            plt.xlabel('degree (Azimuth)')
            plt.ylabel('degree (Elevation)')
            #plt.text(0.9, 0.9, myText, fontsize=14)
            currentFile = os.getcwd()
            #print(currentFile)
            plt.savefig(currentFile+'/Outputs/GazeBallInterpolation/'+str(trialID) +'.png',dpi=800)

            #plt.show()
    error_ballX = np.delete(error_ballX, 0,0)
    error_ballY = np.delete(error_ballY, 0,0)
    error_gazeX = np.delete(error_gazeX, 0,0)
    error_gazeY = np.delete(error_gazeY, 0,0)
    print('Final Size', error_ballY.shape)        
    return error_gazeX, error_gazeY, error_ballX, error_ballY
#myDegree = 4
#postBD = 0.5
for myDegree in [1]:#,2,3,4]:
    for postBD in [0.4]:#, 0.4, 0.5]:

        error_gazeX, error_gazeY, error_ballX, error_ballY = myFunc(myDegree, postBD)
    
        meanGazeX = np.mean(error_gazeX, 0)
        meanGazeY = np.mean(error_gazeY, 0)
        meanBallX = np.mean(error_ballX, 0)
        meanBallY = np.mean(error_ballY, 0)
        
        meanGazeAngle = np.sqrt(np.power(meanGazeX,2) + np.power(meanGazeY,2))
        meanBallAngle = np.sqrt(np.power(meanBallX,2) + np.power(meanBallY,2))
        
        stdGazeX = np.std(error_gazeX, 0)
        stdGazeY = np.std(error_gazeY, 0)
        stdBallX = np.std(error_ballX, 0)
        stdBallY = np.std(error_ballY, 0)
        
        stdGazeAngle = np.sqrt(np.power(stdGazeX,2) + np.power(stdGazeY,2))
        stdBallAngle = np.sqrt(np.power(stdBallX,2) + np.power(stdBallY,2))
        
        t = np.arange(38.0)/.075
        #print('Size',stdHandX.shape,'Value',stdHandX)
        plt.figure(figsize = (9,6))
        #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
        #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
        #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
        
        plt.plot(t, meanGazeAngle,'b',  label='gaze')
        plt.fill_between(t, y1 = meanGazeAngle - stdGazeAngle , y2 = meanGazeAngle + stdGazeAngle ,color = 'b', alpha = 0.6)
        
        plt.plot(t, meanBallAngle,'r',  label='ball')
        plt.fill_between(t, y1 = meanBallAngle - stdBallAngle , y2 = meanBallAngle + stdBallAngle ,color = 'r', alpha = 0.6)
        
        #plt.plot(t, meanGazeX,'b',  label='gaze X')
        #plt.fill_between(t, y1 = meanGazeX - stdGazeX , y2 = meanGazeX + stdGazeX ,color = 'b', alpha = 0.6)
        
        #plt.plot(t, meanGazeY,'r',  label='gaze Y')
        #plt.fill_between(t, y1 = meanGazeY - stdGazeY , y2 = meanGazeY + stdGazeY ,color = 'r', alpha = 0.6)
        
        #plt.plot(t, meanBallX,'c',  label='ball X')
        #plt.fill_between(t, y1 = meanBallX - stdBallX , y2 = meanBallX + stdBallX ,color = 'c', alpha = 0.6)
        
        #plt.plot(t, meanBallY,'y',  label='ball Y')
        #plt.fill_between(t, y1 = meanBallY - stdBallY , y2 = meanBallY + stdBallY ,color = 'y', alpha = 0.6)
        
        #plt.axvline(x=-postBD, color='k', linestyle='--')
        #plt.axvline(x=-postBD - 0.5, color='k', linestyle='--')
        plt.ylim(-1,3)
        plt.grid(True)
        plt.legend(loc=[0.05,0.86], fontsize = 12)
        plt.title("Fitting Polinomial of Degree = "+str(myDegree), fontsize = 20)
        plt.xlabel('Time [s]', fontsize = 20)
        plt.ylabel('Error (degree)', fontsize = 20)
        plt.xticks(fontsize = 20)
        plt.yticks(fontsize = 20)
        currentFile = os.getcwd()
        #print(currentFile)
        plt.savefig(currentFile+'/Outputs/GazeBallInterpolation/PostBD_'+str(postBD)+'_degree_'+str(myDegree)+'.png',dpi=600)
        #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.svg', format='svg', dpi=1000)
        #plt.show()
        #plt.close() 



