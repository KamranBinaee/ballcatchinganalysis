#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 25 21:35:10 2017

@author: kamranbinaee
"""

from __future__ import division

import sys
import PerformParser as pp
import pandas as pd
import numpy as np
import scipy
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


gazeX = np.array([])
gazeY = np.array([])
handX = np.array([])
handY = np.array([])
handZ = np.array([])
myColor = np.array([])

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']

# Best Sbjects
#fileTimeList = [ '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-6-13-4' ]

#fileTimeList = [ '2016-5-3-12-52']#, '2016-5-5-13-7']
fileTimeList = ['2016-5-3-12-52']
# Worst Sbjects
#fileTimeList = ['2016-4-19-14-4', '2016-4-27-13-28', '2016-5-5-13-7']

#fileTimeList = [ '2016-5-6-13-4']

#fileTimeList = ['2016-5-3-12-52']

#fileTimeList = ['2016-5-5-13-7']
#fileTime = '2016-4-19-14-4'
def dotproduct( v1, v2):
    r = sum((a*b) for a, b in zip(v1, v2))
    return r


def length(v):
    return np.sqrt(dotproduct(v, v))


def vectorAngle( v1, v2):
    r = (180.0/np.pi)*np.arccos((dotproduct(v1, v2)) / (length(v1) * length(v2)))#np.arccos((np.dot(v1,v2)/(np.linalg.norm(v1)*np.linalg.norm(v2))))#
    return r

def myPolyfit(x, y, degree):
    # Polynomial Regression
    x = np.array(x, dtype = float)
    y = np.array(y, dtype = float)
    results = {}

    coeffs = np.polyfit(x, y, degree)

     # Polynomial Coefficients
    results['polynomial'] = coeffs.tolist()

    # r-squared
    p = np.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = np.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = np.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = np.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    results['determination'] = ssreg / sstot

    return results

def myFunc(timeOffset):
    gazeX = np.array([])
    gazeY = np.array([])
    handX = np.array([])
    handY = np.array([])
    handZ = np.array([])
    myColor = np.array([])
    eyeToScreenDistance = 0.0725

    for fileTime in fileTimeList: 
        print('.... File Time : ', fileTime)
        expCfgName = "gd_pilot.cfg"
        sysCfgName = "PERFORMVR.cfg"
         
        filePath = "../Data/" + fileTime + "/"
        fileName = "exp_data-" + fileTime
         
        sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
         
        rawDataFrame = sessionDict['raw']
        processedDataFrame = sessionDict['processed']
        calibDataFrame = sessionDict['calibration']
        trialInfoDataFrame = sessionDict['trialInfo']

        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.preBlankDur, trialInfoDataFrame.postBlankDur])
        gb = trialInfoDataFrame.groupby(trialInfoDataFrame.postBlankDur)
        preBDList = [0.6, 0.8, 1.0]
        postBDList = [0.3, 0.4, 0.5]
        postBD = 0.3
        fileName = 'PostBD = '+str(postBD)
        slicedDF = gb.get_group(postBD)
        #slicedDF = trialInfoDataFrame
        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.postBlankDur])
        #slicedDF = gb.get_group((0.3))
        crIndex = slicedDF.ballCrossingIndex.values
        stIndex = slicedDF.trialStartIdx.values
        #ballOnIndex = slicedDF.ballOnIdx.values
        trialID = 1
        #print('Index List1 = ', crIndex)
        #print('Index List2 = ', stIndex)

        bx = processedDataFrame.rotatedBallOnScreen.X.diff()
        bx = np.array(bx, dtype = float)
        #bx = (180/np.pi)*np.arctan(bx/eyeToScreenDistance)
        by = processedDataFrame.rotatedBallOnScreen.Y.diff()
        by = np.array(by, dtype = float)
        #by = (180/np.pi)*np.arctan(by/eyeToScreenDistance)

        gx = processedDataFrame.rotatedGa`ePoint.X.diff()
        gx = np.array(gx, dtype = float)
        #gx = (180/np.pi)*np.arctan(gx/eyeToScreenDistance)
        gy = processedDataFrame.rotatedGazePoint.Y.diff()
        gy = np.array(gy, dtype = float)
        #gy = (180/np.pi)*np.arctan(gy/eyeToScreenDistance)
        
        angle = vectorAngle([bx,by], [gx,gy])
        print('len = ', len(angle))

        x = processedDataFrame.gazePoint.X.values
        x = np.array(x, dtype = float)
        gazeX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
        y = processedDataFrame.gazePoint.Y.values
        y = np.array(y, dtype = float)
        gazeY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)

        x = processedDataFrame.ballOnScreen.X.values
        x = np.array(x, dtype = float)
        ballX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
        #y = processedDataFrame.rotatedBallOnScreen.Y.values[stIndex[trialID]:crIndex[trialID]]
        y = processedDataFrame.ballOnScreen.Y.values
        y = np.array(y, dtype = float)
        ballY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
        print('Success?', slicedDF.ballCaughtQ.values[trialID])
        idx = slicedDF.ballCrossingIndex.values[trialID]
        print('GEX = ', ballX[idx] - gazeX[idx])
        print('GEY = ', ballY[idx] - gazeY[idx])
        idx = slicedDF.ballCrossingIndex.values
        print('\nMinX = ', min(ballX[idx] - gazeX[idx]))
        print('MeanX = ', np.mean(ballX[idx] - gazeX[idx]))
        print('STDX = ', np.std(ballX[idx] - gazeX[idx]))
        print('MaxX = ', max(ballX[idx] - gazeX[idx]))
        print('Rate = ', sum(slicedDF.ballCaughtQ.values)/0.45)

        idx = slicedDF.ballCrossingIndex.values[trialID]
        frames = np.arange(stIndex[trialID],crIndex[trialID]+1)
        xlim = 60
        ylim = 50
        plt.figure()
        plt.plot(ballX[frames], ballY[frames], 'ob')
        plt.plot(gazeX[frames], gazeY[frames], 'xr')
        plt.plot(gazeX[idx], gazeY[idx], '*y')
        plt.plot(ballX[idx], ballY[idx], '*y')
        idx = np.arange(slicedDF.ballOffIdx.values[trialID],slicedDF.ballOnIdx.values[trialID]+1)
        plt.plot(ballX[idx], ballY[idx], '*g')
        plt.plot(gazeX[idx], gazeY[idx], '*g')
        
        
        #plt.xlim(-30,xlim)
        #plt.ylim(-ylim,ylim)
        plt.grid(True)
        myText = 'Success Trial' if (slicedDF.ballCaughtQ.values[trialID] == True) else 'Fail Trial'
        plt.title('Gaze Vs Ball Angular Position : '+ str(myText))
        plt.xlabel('degree (Azimuth)')
        plt.ylabel('degree (Elevation)')
        #plt.text(0.9, 0.9, myText, fontsize=14)
        currentFile = os.getcwd()
        #print(currentFile)
        plt.savefig(currentFile+'/Outputs/ballTrackingSamples/'+str(trialID) +'.png',dpi=800)
        plt.show()
        sys.exit()
        
        i = 1
        x = processedDataFrame.ballOnScreen.X.values[slicedDF.ballOnIdx.values + i] - processedDataFrame.gazePoint.X.values[slicedDF.ballOnIdx.values + i]
        x = np.array(abs(x), dtype = float)
        y = processedDataFrame.ballOnScreen.Y.values[slicedDF.ballOnIdx.values + i] - processedDataFrame.gazePoint.Y.values[slicedDF.ballOnIdx.values + i]
        y = np.array(abs(y), dtype = float)
        
        i = -1
        tempVar1 = np.array(processedDataFrame.cycGazeVelocity.values[slicedDF.ballCrossingIndex.values + i], dtype = float)

        i = 15
        tempVar2 = angle[slicedDF.ballOnIdx.values + i]
        tempVar = tempVar2# + tempVar1 + (180/np.pi)*np.arctan(x/eyeToScreenDistance) + (180/np.pi)*np.arctan(y/eyeToScreenDistance)

        gazeX = np.hstack((gazeX, tempVar))

        xVel = rawDataFrame.paddlePos.X.diff()/(1/75.)
        yVel = rawDataFrame.paddlePos.Y.diff()/(1/75.)
        zVel = rawDataFrame.paddlePos.Z.diff()/(1/75.)

        #handX = np.hstack((handX, xVel[slicedDF.ballCrossingIndex.values+timeOffset]))# - rawDataFrame.ballPos.X.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
        #handY = np.hstack((handY, yVel[slicedDF.ballCrossingIndex.values+timeOffset]))# - rawDataFrame.ballPos.Y.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
        #handZ = np.hstack((handZ, zVel[slicedDF.ballCrossingIndex.values+timeOffset]))# - rawDataFrame.ballPos.Z.values[trialInfoDataFrame.ballCrossingIndex.values+i]))
        handX = np.hstack((handX, rawDataFrame.paddlePos.X.values[slicedDF.ballCrossingIndex.values+timeOffset] - rawDataFrame.ballPos.X.values[slicedDF.ballCrossingIndex.values+timeOffset]))
        handY = np.hstack((handY, rawDataFrame.paddlePos.Y.values[slicedDF.ballCrossingIndex.values+timeOffset] - rawDataFrame.ballPos.Y.values[slicedDF.ballCrossingIndex.values+timeOffset]))
        handZ = np.hstack((handZ, rawDataFrame.paddlePos.Z.values[slicedDF.ballCrossingIndex.values+timeOffset] - rawDataFrame.ballPos.Z.values[slicedDF.ballCrossingIndex.values+timeOffset]))

        myColor = np.hstack((myColor, slicedDF.ballCaughtQ.values))
         
        
    return gazeX, handX, handY, handZ, myColor
gazeX, handX, handY, handZ, myColor = myFunc(0)
#distance = np.power(np.power(handX,2)+np.power(handY,2)+np.power(handZ,2),0.5)

   
    #plt.close()
handX = np.array(handX, dtype = float)
handY = np.array(handY, dtype = float)
handZ = np.array(handZ, dtype = float)
#gE = np.sqrt(np.power(gazeX,2) + np.power(gazeY,2))
hE = np.sqrt(np.power(handX,2) + np.power(handY,2)+ np.power(handZ,2))
print(len(hE))
print(len(gazeX))
myPlot(gazeX, 100*hE, myColor, 'Gaze Prediction Error (Degree)', 'Hand Position Error [cm]', 'Effect of Gaze Prediction Error on Hand Position', 'GazeHandABS_Bad')

sys.exit()
#myPlot(gazeX, 10*handX, myColor, 'Gaze Prediction Error AZ', 'Hand Velocity X [cm/s]', 'Effect of Gaze Prediction (AZ) on Hand Velocity (X)', 'GazeXHandX_Bad')

#myPlot(abs(gazeX), abs(handY), myColor, 'Gaze Prediction Error Az', 'Hand Position Error Y', 'Effect of Gaze Prediction (AZ) on Hand Position (Y)')
#myPlot(abs(gazeX), abs(handZ), myColor, 'Gaze Prediction Error Az', 'Hand Position Error Z', 'Effect of Gaze Prediction (AZ) on Hand Position (Z)')

#gazeX, gazeY, handX, handY, handZ, myColor = myFunc(0)
#myPlot(gazeY, 10*handY, myColor, 'Gaze Prediction Error EL', 'Hand Velocity Y [cm/s]', 'Effect of Gaze Prediction (EL) on Hand Velocity (Y)', 'GazeYHandY_Bad')
#myPlot(gazeX, 10*handX, myColor, 'Gaze Prediction Error AZ', 'Hand Velocity X [cm/s]', 'Effect of Gaze Prediction (AZ) on Hand Velocity (X)', 'GazeXHandX_Bad')

'''
    xlim = 40
    ylim = 40
    plt.figure()
    plt.plot(ballX, ballY, 'ob')
    plt.plot(gazeX, gazeY, 'xr')
    
    plt.xlim(-xlim,xlim)
    plt.ylim(-2,ylim)
    plt.grid(True)
    plt.title('Gaze Vs Ball Angular Position')
    plt.xlabel('degree (Azimuth)')
    plt.ylabel('degree (Elevation)')
    #plt.text(7, 5, r'$R^2 = $' + '%.2f'%(rSquared) + '\n' + r'$m = $'+ '%.2f'%(m[0]), fontsize=14)
    #currentFile = os.getcwd()
    #print(currentFile)
    #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'.png',dpi=800)
    plt.show()

    #xlim = 40
    ylim = 90
    plt.figure()
    plt.plot(np.arange(len(myAngle))*(1/75.),myAngle)
    
    #plt.xlim(-xlim,xlim)
    plt.ylim(0,ylim)
    plt.grid(True)
    plt.title('Gaze Vs Ball Angular Position')
    plt.xlabel('Time')
    plt.ylabel('degree')
    #plt.text(7, 5, r'$R^2 = $' + '%.2f'%(rSquared) + '\n' + r'$m = $'+ '%.2f'%(m[0]), fontsize=14)
    #currentFile = os.getcwd()
    #print(currentFile)
    #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'.png',dpi=800)
    plt.show()


    x = processedDataFrame.ballOnScreen.X.values[stIndex[trialID]:crIndex[trialID]]
    x = np.array(x, dtype = float)
    ballX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
    #y = processedDataFrame.rotatedBallOnScreen.Y.values[stIndex[trialID]:crIndex[trialID]]
    y = processedDataFrame.ballOnScreen.Y.values[stIndex[trialID]:crIndex[trialID]]
    y = np.array(y, dtype = float)
    ballY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
    
    x = processedDataFrame.gazePoint.X.values[stIndex[trialID]:crIndex[trialID]]
    x = np.array(x, dtype = float)
    gazeX = (180/np.pi)*np.arctan(x/eyeToScreenDistance)
    y = processedDataFrame.gazePoint.Y.values[stIndex[trialID]:crIndex[trialID]]
    y = np.array(y, dtype = float)
    gazeY = (180/np.pi)*np.arctan(y/eyeToScreenDistance)
    print ('Len = ', len(gazeX))
    print ('Success? ', slicedDF.ballCaughtQ.values[trialID])
    print ('LLLLEN = ', len(slicedDF.ballOnIdx.values))

'''
