#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 23:58:19 2017

@author: kamranbinaee
"""

from __future__ import division

import PerformParser as pp
import pandas as pd
import numpy as np
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


def reject_outliers(data):
    m = 2
    u = np.nanmean(data)
    s = np.nanstd(data)
    filtered = [e for e in data if (u - 2 * s < e < u + 2 * s)]
    return filtered

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']
 
fileTime = '2016-5-3-12-52'
 
expCfgName = "gd_pilot.cfg"
sysCfgName = "PERFORMVR.cfg"
 
filePath = "../Data/" + fileTime + "/"
fileName = "exp_data-" + fileTime
 
sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
 
rawDataFrame = sessionDict['raw']
processedDataFrame = sessionDict['processed']
calibDataFrame = sessionDict['calibration']
trialInfoDataFrame = sessionDict['trialInfo']

#==============================================================================

#==============================================================================
#print ('Reading the All Subject Pickle File')
#df = pd.read_pickle('../Data/AllSubjects_2.pickle')
#rawDataFrame = df['raw']
#processedDataFrame = df['processed']
#calibDataFrame = df['calibration']
#trialInfoDataFrame = df['trialInfo']
# 
#==============================================================================
xVel = rawDataFrame.paddlePos.X.diff()/(1/75.)
yVel = rawDataFrame.paddlePos.Y.diff()/(1/75.)
zVel = rawDataFrame.paddlePos.Z.diff()/(1/75.)

gb = trialInfoDataFrame.groupby([trialInfoDataFrame.preBlankDur, trialInfoDataFrame.postBlankDur])
preBDList = [0.6, 0.8, 1.0]
postBDList = [0.3, 0.4, 0.5]
for preBD in preBDList:
    for postBD in postBDList:
        plt.figure()
        fileName = 'PreBD = '+str(preBD)+' PostBD = '+str(postBD)
        slicedDF = gb.get_group((preBD,postBD))
        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.postBlankDur])
        #slicedDF = gb.get_group((0.3))
        crIndex = slicedDF.ballCrossingIndex.values
        stIndex = slicedDF.trialStartIdx
        averageTTC = int(np.mean(slicedDF.ballCrossingIndex.values - slicedDF.trialStartIdx))
        print('Average TTC = ',averageTTC/75.0, int(averageTTC))
        xPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        yPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        zPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        #xPos.reshape((len(slicedDF),averageTTC))
        #print(xPos)
        for cnt in range(len(slicedDF) - 1):
            i = slicedDF.trialStartIdx.values[cnt]
            j = slicedDF.ballCrossingIndex.values[cnt]
            t = np.arange(averageTTC)/75.
            
            xVel[i] = 0
            yVel[i] = 0
            zVel[i] = 0
            x = xVel[i:j]
            y = yVel[i:j]
            z = zVel[i:j]
            
            print('Len Before', len(x))
            if(len(x)<averageTTC):
                for i in range(averageTTC - len(x)):
                    x = np.hstack((x,np.NaN))
                    y = np.hstack((y,np.NaN))
                    z = np.hstack((z,np.NaN))
            print('Len After', len(x))
            xPos[cnt,:] = x[:averageTTC]
            yPos[cnt,:] = y[:averageTTC]
            zPos[cnt,:] = z[:averageTTC]
        #print(xPos.shape)
        #print(yPos.shape)
        #print(zPos.shape)
        
        #xPos = reject_outliers(xPos[:])
        #yPos = reject_outliers(yPos[:])
        #zPos = reject_outliers(zPos[:])
        
        meanHandX = np.nanmean(xPos,0)
        meanHandY = np.nanmean(yPos,0)
        meanHandZ = np.nanmean(zPos,0)
        
        stdHandX = np.nanstd(xPos,0)
        stdHandY = np.nanstd(yPos,0)
        stdHandZ = np.nanstd(zPos,0)
        print('Size',stdHandX.shape,'Value',stdHandX)
        plt.figure()
        plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
        #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
        #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
        plt.ylim(-0.5,4)
        plt.grid(True)
        plt.legend()
        plt.title("Hand Velocity X Vs. Time"+"\n"+fileName)
        plt.xlabel('Time [s]')
        plt.ylabel('Hand Velocity[m/s]')
        currentFile = os.getcwd()
        print(currentFile)
        plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_X.png')
        #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_X.svg', format='svg', dpi=1000)
        #plt.show()
        plt.close()

for preBD in preBDList:
    for postBD in postBDList:
        plt.figure()
        fileName = 'PreBD = '+str(preBD)+' PostBD = '+str(postBD)
        slicedDF = gb.get_group((preBD,postBD))
        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.postBlankDur])
        #slicedDF = gb.get_group((0.3))
        crIndex = slicedDF.ballCrossingIndex.values
        stIndex = slicedDF.trialStartIdx
        averageTTC = int(np.mean(slicedDF.ballCrossingIndex.values - slicedDF.trialStartIdx))
        print('Average TTC = ',averageTTC/75.0, int(averageTTC))
        xPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        yPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        zPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        #xPos.reshape((len(slicedDF),averageTTC))
        print(xPos)
        for cnt in range(len(slicedDF) - 1):
            i = slicedDF.trialStartIdx.values[cnt]
            j = slicedDF.ballCrossingIndex.values[cnt]
            t = np.arange(averageTTC)/75.
            
            xVel[i] = 0
            yVel[i] = 0
            zVel[i] = 0
            x = xVel[i:j]
            y = yVel[i:j]
            z = zVel[i:j]
            
            print('Len Before', len(x))
            if(len(x)<averageTTC):
                for i in range(averageTTC - len(x)):
                    x = np.hstack((x,np.NaN))
                    y = np.hstack((y,np.NaN))
                    z = np.hstack((z,np.NaN))
            print('Len After', len(x))
            xPos[cnt,:] = x[:averageTTC]
            yPos[cnt,:] = y[:averageTTC]
            zPos[cnt,:] = z[:averageTTC]
        #print(xPos.shape)
        #print(yPos.shape)
        #print(zPos.shape)

        #xPos = reject_outliers(xPos)
        #yPos = reject_outliers(yPos)
        #zPos = reject_outliers(zPos)
        
        meanHandX = np.nanmean(xPos,0)
        meanHandY = np.nanmean(yPos,0)
        meanHandZ = np.nanmean(zPos,0)
        
        stdHandX = np.nanstd(xPos,0)
        stdHandY = np.nanstd(yPos,0)
        stdHandZ = np.nanstd(zPos,0)
        print('Size',stdHandX.shape,'Value',stdHandX)
        plt.figure()
        #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
        plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
        #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
        plt.ylim(-4,2.5)
        plt.grid(True)
        plt.legend()
        plt.title("Hand Velocity Y Vs. Time"+"\n"+fileName)
        plt.xlabel('Time [s]')
        plt.ylabel('Hand Velocity[m/s]')
        currentFile = os.getcwd()
        print(currentFile)
        plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Y.png')
        #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Y.svg', format='svg', dpi=1000)
        #plt.show()
        plt.close()

for preBD in preBDList:
    for postBD in postBDList:
        plt.figure()
        fileName = 'PreBD = '+str(preBD)+' PostBD = '+str(postBD)
        slicedDF = gb.get_group((preBD,postBD))
        #gb = trialInfoDataFrame.groupby([trialInfoDataFrame.postBlankDur])
        #slicedDF = gb.get_group((0.3))
        crIndex = slicedDF.ballCrossingIndex.values
        stIndex = slicedDF.trialStartIdx
        averageTTC = int(np.mean(slicedDF.ballCrossingIndex.values - slicedDF.trialStartIdx))
        print('Average TTC = ',averageTTC/75.0, int(averageTTC))
        xPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        yPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        zPos = np.zeros((len(slicedDF),averageTTC),dtype = float)
        #xPos.reshape((len(slicedDF),averageTTC))
        print(xPos)
        for cnt in range(len(slicedDF) - 1):
            i = slicedDF.trialStartIdx.values[cnt]
            j = slicedDF.ballCrossingIndex.values[cnt]
            t = np.arange(averageTTC)/75.
            
            xVel[i] = 0
            yVel[i] = 0
            zVel[i] = 0
            x = xVel[i:j]
            y = yVel[i:j]
            z = zVel[i:j]
            
            print('Len Before', len(x))
            if(len(x)<averageTTC):
                for i in range(averageTTC - len(x)):
                    x = np.hstack((x,np.NaN))
                    y = np.hstack((y,np.NaN))
                    z = np.hstack((z,np.NaN))
            print('Len After', len(x))
            xPos[cnt,:] = x[:averageTTC]
            yPos[cnt,:] = y[:averageTTC]
            zPos[cnt,:] = z[:averageTTC]
        #print(xPos.shape)
        #print(yPos.shape)
        #print(zPos.shape)

        #xPos = reject_outliers(xPos)
        #yPos = reject_outliers(yPos)
        #zPos = reject_outliers(zPos)
        
        meanHandX = np.nanmean(xPos,0)
        meanHandY = np.nanmean(yPos,0)
        meanHandZ = np.nanmean(zPos,0)
        
        stdHandX = np.nanstd(xPos,0)
        stdHandY = np.nanstd(yPos,0)
        stdHandZ = np.nanstd(zPos,0)
        print('Size',stdHandX.shape,'Value',stdHandX)
        plt.figure()
        #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
        #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
        plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
        plt.ylim(-1.75,1.25)
        plt.grid(True)
        plt.legend()
        plt.title("Hand Velocity Z Vs. Time"+"\n"+fileName)
        plt.xlabel('Time [s]')
        plt.ylabel('Hand Velocity[m/s]')
        currentFile = os.getcwd()
        print(currentFile)
        plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.png')
        #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.svg', format='svg', dpi=1000)
        #plt.show()
        plt.close() 
