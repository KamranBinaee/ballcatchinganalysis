#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 16:40:26 2017

@author: kamranbinaee
"""


from __future__ import division

import PerformParser as pp
import pandas as pd
import numpy as np
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


def reject_outliers(data):
    m = 2
    u = np.nanmean(data)
    s = np.nanstd(data)
    filtered = [e for e in data if (u - 2 * s < e < u + 2 * s)]
    return filtered

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']
 
fileTime = '2016-5-4-13-3'
 
expCfgName = "gd_pilot.cfg"
sysCfgName = "PERFORMVR.cfg"
 
filePath = "../Data/" + fileTime + "/"
fileName = "exp_data-" + fileTime
 
sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
 
rawDataFrame = sessionDict['raw']
processedDataFrame = sessionDict['processed']
calibDataFrame = sessionDict['calibration']
trialInfoDataFrame = sessionDict['trialInfo']

#==============================================================================

#==============================================================================
#print ('Reading the All Subject Pickle File')
#df = pd.read_pickle('../Data/AllSubjects_2.pickle')
#rawDataFrame = df['raw']
#processedDataFrame = df['processed']
#calibDataFrame = df['calibration']
#trialInfoDataFrame = df['trialInfo']
# 
#==============================================================================
xVel = rawDataFrame.paddlePos.X.diff()/(1/75.)
yVel = rawDataFrame.paddlePos.Y.diff()/(1/75.)
zVel = rawDataFrame.paddlePos.Z.diff()/(1/75.)

gb = trialInfoDataFrame.groupby(trialInfoDataFrame.postBlankDur)
preBDList = [0.6, 0.8, 1.0]
postBDList = [0.3, 0.4, 0.5]

#==============================================================================
#  Uncomment this section of the code to plot the Absolute hand Velocity 
#  This generates only one plot for each PostBD (mean and std)
#==============================================================================

for postBD in postBDList:
    plt.figure()
    fileName = 'PostBD = '+str(postBD)
    slicedDF = gb.get_group(postBD)
    averageTTC = 101
    #print('Average TTC = ',averageTTC/75.0, int(averageTTC))
    xPos = np.zeros((len(slicedDF),101),dtype = float)
    yPos = np.zeros((len(slicedDF),101),dtype = float)
    zPos = np.zeros((len(slicedDF),101),dtype = float)
    myVelocity = np.zeros((len(slicedDF),101),dtype = float)
    
    offsetPlus = int(postBD*75)
    offsetMinus = 101 - offsetPlus# 37.5
    #print (offsetPlus, offsetMinus)
    for cnt in range(len(slicedDF)):
        #i = slicedDF.trialStartIdx.values[cnt]
        #i = slicedDF.ballOffIdx.values[cnt]
        j = slicedDF.ballOnIdx.values[cnt]
        jj= slicedDF.ballOffIdx.values[cnt]
        #t = (np.arange(-averageTTC,1))/75.
        t = (np.arange(-100,1))/75
        x = xVel[j-offsetMinus:j+offsetPlus]
        y = yVel[j-offsetMinus:j+offsetPlus]
        z = zVel[j-offsetMinus:j+offsetPlus]
        #print(cnt)
#        print('Len Before', len(x))
#        if(len(x)<averageTTC):
#            for i in range(averageTTC - len(x)):
#                x = np.hstack((x,np.NaN))
#                y = np.hstack((y,np.NaN))
#                z = np.hstack((z,np.NaN))
        #print('Len After', len(x))
        xPos[cnt,:] = x#[:averageTTC]
        yPos[cnt,:] = y#[:averageTTC]
        zPos[cnt,:] = z#[:averageTTC]
        myVelocity[cnt,:] = np.sqrt(np.array(np.power(x,2)+np.power(y,2)+np.power(z,2), dtype = float))

    #print(xPos.shape)
    #print(yPos.shape)
    #print(zPos.shape)
    
    #xPos = reject_outliers(xPos[:])
    #yPos = reject_outliers(yPos[:])
    #zPos = reject_outliers(zPos[:])
    
    meanHandX = np.nanmean(xPos,0)
    meanHandY = np.nanmean(yPos,0)
    meanHandZ = np.nanmean(zPos,0)
    meanVel = np.nanmean(myVelocity,0)
    
    stdHandX = np.nanstd(xPos,0)
    stdHandY = np.nanstd(yPos,0)
    stdHandZ = np.nanstd(zPos,0)
    stdVel = np.nanstd(myVelocity,0)
    
    #print('Size',stdHandX.shape,'Value',stdHandX)
    plt.figure()
    #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
    #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
    #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
    plt.plot(t, meanVel,'b',  label='Hand Velocity')
    plt.fill_between(t, y1 = meanVel - stdVel , y2 = meanVel + stdVel ,color = 'b', alpha = 0.6)
    plt.axvline(x=-postBD, color='k', linestyle='--')
    plt.axvline(x=-postBD - 0.5, color='k', linestyle='--')
    plt.ylim(0,6)
    plt.grid(True)
    plt.legend(loc=[0.7,1])
    plt.title("Hand Velocity Vs. Time"+"\n"+fileName)
    plt.xlabel('Time [s]')
    plt.ylabel('Hand Velocity[m/s]')
    currentFile = os.getcwd()
    #print(currentFile)
    plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_V.png',dpi=600)
    #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.svg', format='svg', dpi=1000)
    #plt.show()
    plt.close() 


#==============================================================================
#  Uncomment this section of the code to plot the hand Velocity in X, Y, Z
#  This generates three plots for each PostBD (mean and std)
#==============================================================================
# for postBD in postBDList:
#     plt.figure()
#     fileName = 'PostBD = '+str(postBD)
#     slicedDF = gb.get_group(postBD)
#     averageTTC = 101
#     #print('Average TTC = ',averageTTC/75.0, int(averageTTC))
#     xPos = np.zeros((len(slicedDF),101),dtype = float)
#     yPos = np.zeros((len(slicedDF),101),dtype = float)
#     zPos = np.zeros((len(slicedDF),101),dtype = float)
#     offsetPlus = int(postBD*75)
#     offsetMinus = 101 - offsetPlus# 37.5
#     #print (offsetPlus, offsetMinus)
#     for cnt in range(len(slicedDF)):
#         #i = slicedDF.trialStartIdx.values[cnt]
#         #i = slicedDF.ballOffIdx.values[cnt]
#         j = slicedDF.ballOnIdx.values[cnt]
#         #t = (np.arange(-averageTTC,1))/75.
#         t = (np.arange(-100,1))/75
#         x = xVel[j-offsetMinus:j+offsetPlus]
#         y = yVel[j-offsetMinus:j+offsetPlus]
#         z = zVel[j-offsetMinus:j+offsetPlus]
#         #print(cnt)
#         
# #        print('Len Before', len(x))
# #        if(len(x)<averageTTC):
# #            for i in range(averageTTC - len(x)):
# #                x = np.hstack((x,np.NaN))
# #                y = np.hstack((y,np.NaN))
# #                z = np.hstack((z,np.NaN))
#         #print('Len After', len(x))
#         xPos[cnt,:] = x#[:averageTTC]
#         yPos[cnt,:] = y#[:averageTTC]
#         zPos[cnt,:] = z#[:averageTTC]
#     #print(xPos.shape)
#     #print(yPos.shape)
#     #print(zPos.shape)
#     
#     #xPos = reject_outliers(xPos[:])
#     #yPos = reject_outliers(yPos[:])
#     #zPos = reject_outliers(zPos[:])
#     
#     meanHandX = np.nanmean(xPos,0)
#     meanHandY = np.nanmean(yPos,0)
#     meanHandZ = np.nanmean(zPos,0)
#     
#     stdHandX = np.nanstd(xPos,0)
#     stdHandY = np.nanstd(yPos,0)
#     stdHandZ = np.nanstd(zPos,0)
#     #print('Size',stdHandX.shape,'Value',stdHandX)
#     plt.figure()
#     #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
#     plt.plot(t, meanHandX,'b',  label='Hand Velocity X')
#     plt.fill_between(t, y1 = meanHandX - stdHandX , y2 = meanHandX + stdHandX ,color = 'b', alpha = 0.6)
#     plt.axvline(x=-postBD, color='k', linestyle='--')
#     plt.axvline(x=-postBD - 0.5, color='k', linestyle='--')
#     #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
#     #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
#     plt.ylim(-2.5,4)
#     plt.grid(True)
#     plt.legend(loc=[0.7,1])
#     plt.title("Hand Velocity X Vs. Time"+"\n"+fileName)
#     plt.xlabel('Time [s]')
#     plt.ylabel('Hand Velocity[m/s]')
#     currentFile = os.getcwd()
#     #print(currentFile)
#     plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_X.png',dpi=600)
#     #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_X.svg', format='svg', dpi=1000)
#     #plt.show()
#     plt.close()
# 
# for postBD in postBDList:
#     plt.figure()
#     fileName = 'PostBD = '+str(postBD)
#     slicedDF = gb.get_group(postBD)
#     averageTTC = 101
#     #print('Average TTC = ',averageTTC/75.0, int(averageTTC))
#     xPos = np.zeros((len(slicedDF),101),dtype = float)
#     yPos = np.zeros((len(slicedDF),101),dtype = float)
#     zPos = np.zeros((len(slicedDF),101),dtype = float)
#     offsetPlus = int(postBD*75)
#     offsetMinus = 101 - offsetPlus# 37.5
#     #print (offsetPlus, offsetMinus)
#     for cnt in range(len(slicedDF)):
#         #i = slicedDF.trialStartIdx.values[cnt]
#         #i = slicedDF.ballOffIdx.values[cnt]
#         j = slicedDF.ballOnIdx.values[cnt]
#         #t = (np.arange(-averageTTC,1))/75.
#         t = (np.arange(-100,1))/75
#         x = xVel[j-offsetMinus:j+offsetPlus]
#         y = yVel[j-offsetMinus:j+offsetPlus]
#         z = zVel[j-offsetMinus:j+offsetPlus]
#         #print(cnt)       
# #        print('Len Before', len(x))
# #        if(len(x)<averageTTC):
# #            for i in range(averageTTC - len(x)):
# #                x = np.hstack((x,np.NaN))
# #                y = np.hstack((y,np.NaN))
# #                z = np.hstack((z,np.NaN))
#         #print('Len After', len(x))
#         xPos[cnt,:] = x#[:averageTTC]
#         yPos[cnt,:] = y#[:averageTTC]
#         zPos[cnt,:] = z#[:averageTTC]
#     #print(xPos.shape)
#     #print(yPos.shape)
#     #print(zPos.shape)
#     
#     #xPos = reject_outliers(xPos[:])
#     #yPos = reject_outliers(yPos[:])
#     #zPos = reject_outliers(zPos[:])
#     
#     meanHandX = np.nanmean(xPos,0)
#     meanHandY = np.nanmean(yPos,0)
#     meanHandZ = np.nanmean(zPos,0)
#     
#     stdHandX = np.nanstd(xPos,0)
#     stdHandY = np.nanstd(yPos,0)
#     stdHandZ = np.nanstd(zPos,0)
#     #print('Size',stdHandX.shape,'Value',stdHandX)
#     plt.figure()
#     #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
#     #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
#     plt.plot(t, meanHandY,'r',  label='Hand Velocity Y')
#     plt.fill_between(t, y1 = meanHandY - stdHandY , y2 = meanHandY + stdHandY ,color = 'r', alpha = 0.6)
#     plt.axvline(x=-postBD, color='k', linestyle='--')
#     plt.axvline(x=-postBD - 0.5, color='k', linestyle='--')
#     
#     #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
#     plt.ylim(-4,2.5)
#     plt.grid(True)
#     plt.legend(loc=[0.7,1])
#     plt.title("Hand Velocity Y Vs. Time"+"\n"+fileName)
#     plt.xlabel('Time [s]')
#     plt.ylabel('Hand Velocity[m/s]')
#     currentFile = os.getcwd()
#     #print(currentFile)
#     plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Y.png',dpi=600)
#     #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Y.svg', format='svg', dpi=1000)
#     #plt.show()
#     plt.close()
# 
# for postBD in postBDList:
#     plt.figure()
#     fileName = 'PostBD = '+str(postBD)
#     slicedDF = gb.get_group(postBD)
#     averageTTC = 101
#     #print('Average TTC = ',averageTTC/75.0, int(averageTTC))
#     xPos = np.zeros((len(slicedDF),101),dtype = float)
#     yPos = np.zeros((len(slicedDF),101),dtype = float)
#     zPos = np.zeros((len(slicedDF),101),dtype = float)
#     offsetPlus = int(postBD*75)
#     offsetMinus = 101 - offsetPlus# 37.5
#     #print (offsetPlus, offsetMinus)
#     for cnt in range(len(slicedDF)):
#         #i = slicedDF.trialStartIdx.values[cnt]
#         #i = slicedDF.ballOffIdx.values[cnt]
#         j = slicedDF.ballOnIdx.values[cnt]
#         #t = (np.arange(-averageTTC,1))/75.
#         t = (np.arange(-100,1))/75
#         x = xVel[j-offsetMinus:j+offsetPlus]
#         y = yVel[j-offsetMinus:j+offsetPlus]
#         z = zVel[j-offsetMinus:j+offsetPlus]
#         #print(cnt)
# #        print('Len Before', len(x))
# #        if(len(x)<averageTTC):
# #            for i in range(averageTTC - len(x)):
# #                x = np.hstack((x,np.NaN))
# #                y = np.hstack((y,np.NaN))
# #                z = np.hstack((z,np.NaN))
#         #print('Len After', len(x))
#         xPos[cnt,:] = x#[:averageTTC]
#         yPos[cnt,:] = y#[:averageTTC]
#         zPos[cnt,:] = z#[:averageTTC]
#     #print(xPos.shape)
#     #print(yPos.shape)
#     #print(zPos.shape)
#     
#     #xPos = reject_outliers(xPos[:])
#     #yPos = reject_outliers(yPos[:])
#     #zPos = reject_outliers(zPos[:])
#     
#     meanHandX = np.nanmean(xPos,0)
#     meanHandY = np.nanmean(yPos,0)
#     meanHandZ = np.nanmean(zPos,0)
#     
#     stdHandX = np.nanstd(xPos,0)
#     stdHandY = np.nanstd(yPos,0)
#     stdHandZ = np.nanstd(zPos,0)
#     #print('Size',stdHandX.shape,'Value',stdHandX)
#     plt.figure()
#     #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
#     #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
#     #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
#     plt.plot(t, meanHandZ,'g',  label='Hand Velocity Z')
#     plt.fill_between(t, y1 = meanHandZ - stdHandZ , y2 = meanHandZ + stdHandZ ,color = 'g', alpha = 0.6)
#     plt.axvline(x=-postBD, color='k', linestyle='--')
#     plt.axvline(x=-postBD - 0.5, color='k', linestyle='--')
#     plt.ylim(-3.5,3)
#     plt.grid(True)
#     plt.legend(loc=[0.7,1])
#     plt.title("Hand Velocity Z Vs. Time"+"\n"+fileName)
#     plt.xlabel('Time [s]')
#     plt.ylabel('Hand Velocity[m/s]')
#     currentFile = os.getcwd()
#     #print(currentFile)
#     plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.png',dpi=600)
#     #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.svg', format='svg', dpi=1000)
#     #plt.show()
#     plt.close() 
# 
#==============================================================================
