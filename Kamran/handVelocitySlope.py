#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 00:33:57 2018

@author: kamranbinaee
"""

from __future__ import division

import PerformParser as pp
import pandas as pd
import numpy as np
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


def reject_outliers(data):
    m = 2
    u = np.nanmean(data)
    s = np.nanstd(data)
    filtered = [e for e in data if (u - 2 * s < e < u + 2 * s)]
    return filtered

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']
 
#fileTimeList = ['2016-5-3-12-52', '2016-4-22-11-57']
colorList = ['b','r','g','c','m','y','k','w','b','r','g',]
subjectCounter = 0
slope = []
succ = []
color = list()
marker = list()
for fileTime in fileTimeList:
     
    expCfgName = "gd_pilot.cfg"
    sysCfgName = "PERFORMVR.cfg"
     
    filePath = "../Data/" + fileTime + "/"
    fileName = "exp_data-" + fileTime
     
    sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
     
    rawDataFrame = sessionDict['raw']
    processedDataFrame = sessionDict['processed']
    calibDataFrame = sessionDict['calibration']
    trialInfoDataFrame = sessionDict['trialInfo']
    #==============================================================================
    
    xVel = rawDataFrame.paddlePos.X.diff()/(1/75.)
    yVel = rawDataFrame.paddlePos.Y.diff()/(1/75.)
    zVel = rawDataFrame.paddlePos.Z.diff()/(1/75.)
    
    gb = trialInfoDataFrame.groupby(trialInfoDataFrame.postBlankDur)
    preBDList = [0.6, 0.8, 1.0]
    postBDList = [0.3, 0.4, 0.5]
    
    #==============================================================================
    #  Uncomment this section to plot slope of hand velocity during blank 
    #  This will take two samples, one at ball off and one at ball off
    #  based on these two it calculates the slop of hand velocity
    #==============================================================================
    for postBD in postBDList:
        plt.figure()
        fileName = 'PostBD = '+str(postBD)
        slicedDF = gb.get_group(postBD)
        successRate = sum(slicedDF.ballCaughtQ.values)/45.
        print('Success Rate = %',sum(trialInfoDataFrame.ballCaughtQ.values)/1.35)
        averageTTC = 101
        #print('Average TTC = ',averageTTC/75.0, int(averageTTC))
        xPos = np.zeros((len(slicedDF),101),dtype = float)
        yPos = np.zeros((len(slicedDF),101),dtype = float)
        zPos = np.zeros((len(slicedDF),101),dtype = float)
        myVelocity = np.zeros((len(slicedDF),101),dtype = float)
        myArray = np.zeros((len(slicedDF),2),dtype = float)
        
        offsetPlus = int(postBD*75)
        offsetMinus = 101 - offsetPlus# 37.5
        #print (offsetPlus, offsetMinus)
        for cnt in range(len(slicedDF)):
            j = slicedDF.ballOnIdx.values[cnt]
            jj= slicedDF.ballOffIdx.values[cnt]
            #t = (np.arange(-averageTTC,1))/75.
            t = (np.arange(-100,1))/75
            x = xVel[j-offsetMinus:j+offsetPlus]
            y = yVel[j-offsetMinus:j+offsetPlus]
            z = zVel[j-offsetMinus:j+offsetPlus]
            
            myVelocity[cnt,:] = np.sqrt(np.array(np.power(x,2)+np.power(y,2)+np.power(z,2), dtype = float))
            # Calculating the Velocity at Ball off Frame
            vx = xVel[jj]
            vy = yVel[jj]
            vz = zVel[jj]
            myArray[cnt,0] = np.sqrt(np.array(np.power(vx,2)+np.power(vy,2)+np.power(vz,2), dtype = float))
    
            # Calculating the Velocity at Ball on Frame
            vx = xVel[j]
            vy = yVel[j]
            vz = zVel[j]
            myArray[cnt,1] = np.sqrt(np.array(np.power(vx,2)+np.power(vy,2)+np.power(vz,2), dtype = float))
        #print(xPos.shape)
        #print(yPos.shape)
        #print(zPos.shape)
        
        #xPos = reject_outliers(xPos[:])
        #yPos = reject_outliers(yPos[:])
        #zPos = reject_outliers(zPos[:])
        
        meanVel = np.nanmean(myArray,0)
        slope.append((meanVel[1] - meanVel[0])*2)
        succ.append(successRate)
        marker.append("o")
        if (postBD == 0.3):
            color.append("r")
        elif(postBD == 0.4):
            color.append("b")
        else:
            color.append("g")
print(color)
print(succ)
print(slope)
print(slope[0::3])
print(slope[1::3])
print(slope[2::3])
succ = np.array(succ)*100.0
print(marker)
plt.figure()
plt.plot(slope[0::3], succ[0::3], 'or', label = 'Post BD = 0.3')
m,b = np.polyfit(slope[0::3], succ[0::3], 1)
x = np.arange(-1.5,2,0.5)
plt.plot(x, m*x+b, '--r')

plt.plot(slope[1::3], succ[1::3], 'oy', label = 'Post BD = 0.4')
m,b = np.polyfit(slope[1::3], succ[1::3], 1) 
x = np.arange(-1.5,2,0.5)
plt.plot(x, m*x+b, '--y')

plt.plot(slope[2::3], succ[2::3], 'oc', label = 'Post BD = 0.5')
m,b = np.polyfit(slope[2::3], succ[2::3], 1) 
x = np.arange(-1.5,2,0.5)
plt.plot(x, m*x+b, '--c')

plt.ylim(0,110)
plt.xlim(-2,2)
plt.grid(True)
plt.legend(loc=[0.05,0.75])
plt.title("Success Rate Vs. Hand Velocity Slope")
plt.xlabel('Hand Velocity Slope')
plt.ylabel('Success Rate')
currentFile = os.getcwd()
#print(currentFile)
plt.savefig(currentFile+'/Outputs/HandVelocityFigures/SuccessRate.png',dpi=600)
#plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.svg', format='svg', dpi=1000)
#plt.show()
plt.close() 

#        
#        stdHandX = np.nanstd(xPos,0)
#        stdHandY = np.nanstd(yPos,0)
#        stdHandZ = np.nanstd(zPos,0)
#        stdVel = np.nanstd(myVelocity,0)
#        
#        #print('Size',stdHandX.shape,'Value',stdHandX)
#        plt.figure()
#        plt.axvline(x=-postBD, color='k', linestyle='--')
#        plt.axvline(x=-postBD - 0.5, color='k', linestyle='--')
#        #plt.errorbar(t, meanHandX, yerr=stdHandX, fmt='--bo', ecolor='b', label='Hand X')
#        #plt.errorbar(t, meanHandY, yerr=stdHandY, fmt='--ro', ecolor='r', label='Hand Y')
#        #plt.errorbar(t, meanHandZ, yerr=stdHandZ,fmt='--go', ecolor='g', label='Hand Z')
#        myX = np.zeros(len(slicedDF))
#        plt.plot(myX-postBD - 0.5, myVelocity[:,-int(postBD*75+0.5*75)],'.b')
#        plt.plot(myX-postBD, myVelocity[:,-int(postBD*75)],'.g')
#        plt.plot(-postBD - 0.5, meanVel[0],'*r')
#        plt.plot(-postBD, meanVel[1],'*r')
#        plt.plot([-postBD - 0.5,-postBD], [meanVel[0], meanVel[1]],'r')
#
#        meanVel = np.nanmean(myVelocity,0)
#        
#        #plt.plot(t, meanVel,'b',  label='Hand Velocity')
#        plt.fill_between(t, y1 = meanVel - stdVel , y2 = meanVel + stdVel ,color = 'b', alpha = 0.6)
#        plt.ylim(0,6)
#        plt.grid(True)
#        plt.legend(loc=[0.7,1])
#        plt.title("Hand Velocity Vs. Time"+"\n"+fileName)
#        plt.xlabel('Time [s]')
#        plt.ylabel('Hand Velocity[m/s]')
#        currentFile = os.getcwd()
#        #print(currentFile)
#        plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Slope.png',dpi=600)
#        #plt.savefig(currentFile+'/Outputs/HandVelocityFigures/'+fileName +'_Z.svg', format='svg', dpi=1000)
#        #plt.show()
#        plt.close() 
#        print(slope[-1])
#    
