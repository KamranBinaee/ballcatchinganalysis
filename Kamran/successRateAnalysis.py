#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 17:40:42 2017

@author: kamranbinaee
"""

from __future__ import division

import PerformParser as pp
import pandas as pd
import numpy as np
from scipy import signal as sig
import performFun as pF

import cv2
import os
import scipy.io as sio
import matplotlib

#%matplotlib notebook
import Quaternion as qu
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.fftpack import fft
from mpl_toolkits.mplot3d import Axes3D


def reject_outliers(data):
    m = 2
    u = np.nanmean(data)
    s = np.nanstd(data)
    filtered = [e for e in data if (u - 2 * s < e < u + 2 * s)]
    return filtered

#================================= To read One Subject Data =============================================
fileTimeList = ['2016-4-19-14-4', '2016-4-22-11-57', '2016-4-27-13-28', '2016-4-28-10-57', '2016-4-29-11-56',
 '2016-5-3-12-52', '2016-5-4-13-3', '2016-5-5-13-7', '2016-5-6-11-2', '2016-5-6-13-4']
 
#fileTime = '2016-4-19-14-4'

x = np.arange(0, 10 * len(fileTimeList), 10)  # the x locations for the groups
width = 2       # the width of the bars
fig, ax = plt.subplots()

tor3 = list()
tor4 = list()
tor5 = list()

for fileTime in fileTimeList:
    expCfgName = "gd_pilot.cfg"
    sysCfgName = "PERFORMVR.cfg"
     
    filePath = "../Data/" + fileTime + "/"
    fileName = "exp_data-" + fileTime
     
    sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
     
    rawDataFrame = sessionDict['raw']
    processedDataFrame = sessionDict['processed']
    calibDataFrame = sessionDict['calibration']
    trialInfoDataFrame = sessionDict['trialInfo']
    
    #==============================================================================
    
    #==============================================================================
    #print ('Reading the All Subject Pickle File')
    #df = pd.read_pickle('../Data/AllSubjects_2.pickle')
    #rawDataFrame = df['raw']
    #processedDataFrame = df['processed']
    #calibDataFrame = df['calibration']
    #trialInfoDataFrame = df['trialInfo']
    # 
    #==============================================================================
    print(sum(list(trialInfoDataFrame.ballCaughtQ.values))/len(trialInfoDataFrame.ballCaughtQ.values)*100,' Success Rate')
    
    gb = trialInfoDataFrame.groupby(trialInfoDataFrame.postBlankDur)
    preBDList = [0.6, 0.8, 1.0]
    postBDList = [0.3, 0.4, 0.5]
    print('For Subject:', fileTime, '\n')
    i=0
    for postBD in postBDList:
        #fileName = 'PreBD = '+str(preBD)+' PostBD = '+str(postBD)
        slicedDF = gb.get_group(postBD)
        print(sum(list(slicedDF.ballCaughtQ.values))/len(slicedDF.ballCaughtQ.values)*100,' = Success Rate for TOR = -', postBD)
        rate = sum(list(slicedDF.ballCaughtQ.values))/len(slicedDF.ballCaughtQ.values)*100
        if(i==0):
            tor3.append(rate)
        elif(i==1):
            tor4.append(rate)
        else:
            tor5.append(rate)
        i = i + 1
        


rects1 = ax.bar(x, tor3, width, color='r')
rects2 = ax.bar(x+2, tor4, width, color='g')
rects3 = ax.bar(x+4., tor5, width, color='b')


# add some text for labels, title and axes ticks
ax.set_ylim(0,104)
ax.set_ylabel('Success Rate')
ax.set_title('Success Rate for Different Subjects and Different TORs')
ax.set_xticks(x + 2)
ax.set_xticklabels(('S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S10'))

ax.legend((rects1[0], rects2[0], rects3[0]), ('TOR = -300 ms', 'TOR = -400 ms', 'TOR = -500 ms'), loc = (0.01,0.78))


def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

#autolabel(rects1)
#autolabel(rects2)
#autolabel(rects3)

plt.savefig('SuccessRateTOR.png', dpi=1000)
plt.savefig('SuccessRateTOR.svg', format='svg', dpi=1000)
plt.show()


fig, ax = plt.subplots()

tor3 = list()
tor4 = list()
tor5 = list()

for fileTime in fileTimeList:
    expCfgName = "gd_pilot.cfg"
    sysCfgName = "PERFORMVR.cfg"
     
    filePath = "../Data/" + fileTime + "/"
    fileName = "exp_data-" + fileTime
     
    sessionDict = pF.loadSessionDict(filePath,fileName,expCfgName,sysCfgName,startFresh=False)
     
    rawDataFrame = sessionDict['raw']
    processedDataFrame = sessionDict['processed']
    calibDataFrame = sessionDict['calibration']
    trialInfoDataFrame = sessionDict['trialInfo']
    
    #==============================================================================
    
    #==============================================================================
    #print ('Reading the All Subject Pickle File')
    #df = pd.read_pickle('../Data/AllSubjects_2.pickle')
    #rawDataFrame = df['raw']
    #processedDataFrame = df['processed']
    #calibDataFrame = df['calibration']
    #trialInfoDataFrame = df['trialInfo']
    # 
    #==============================================================================
    print(sum(list(trialInfoDataFrame.ballCaughtQ.values))/len(trialInfoDataFrame.ballCaughtQ.values)*100,' Success Rate')
    
    gb = trialInfoDataFrame.groupby(trialInfoDataFrame.preBlankDur)
    preBDList = [0.6, 0.8, 1.0]
    postBDList = [0.3, 0.4, 0.5]
    print('For Subject:', fileTime, '\n')
    i=0
    for preBD in preBDList:
        #fileName = 'PreBD = '+str(preBD)+' PostBD = '+str(postBD)
        slicedDF = gb.get_group(preBD)
        print(sum(list(slicedDF.ballCaughtQ.values))/len(slicedDF.ballCaughtQ.values)*100,' = Success Rate for PreBD = ', preBD)
        rate = sum(list(slicedDF.ballCaughtQ.values))/len(slicedDF.ballCaughtQ.values)*100
        if(i==0):
            tor3.append(rate)
        elif(i==1):
            tor4.append(rate)
        else:
            tor5.append(rate)
        i = i + 1
        


rects1 = ax.bar(x, tor3, width, color='r')
rects2 = ax.bar(x+2, tor4, width, color='g')
rects3 = ax.bar(x+4., tor5, width, color='b')


# add some text for labels, title and axes ticks
ax.set_ylim(0,104)
ax.set_ylabel('Success Rate')
ax.set_title('Success Rate for Different Subjects and Different Pre-Blank Durations')
ax.set_xticks(x + 2)
ax.set_xticklabels(('S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S10'))

ax.legend((rects1[0], rects2[0], rects3[0]), ('Pre_BD = 600 ms', 'Pre_BD = 800 ms', 'Pre_BD = 1000 ms'), loc = (0.01,0.78))

#autolabel(rects1)
#autolabel(rects2)
#autolabel(rects3)

plt.savefig('SuccessRatePreBD.png', dpi=1000)
plt.savefig('SuccessRatePreBD.svg', format='svg', dpi=1000)
plt.show()

